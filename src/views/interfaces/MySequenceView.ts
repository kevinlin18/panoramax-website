export interface ResponseUserPhotoInterface {
  assets: { thumb: { href: string }; hd: { href: string } }
  properties: { datetime: Date; 'geovisio:status': string }
  id: string
  bbox: number[]
}

export interface ResponseUserPhotoLinksInterface {
  href?: string
  rel?: string
  title?: string
  type?: string
}

export interface UserSequenceInterface {
  id: string
  title: string
  description: string
  license: string
  created: string
  taken: Date
  location: string
  imageCount: number
  duration: string
  camera: string
  cameraModel: string
  status: string
  providers: [{ name: string; roles: [string] }]
  extent: { temporal: { interval: [Date[]] }; spatial: { bbox: string[] } }
  ['geovisio:sorted-by']?: string
}

export interface ResponseUserSequenceInterface extends UserSequenceInterface {
  ['stats:items']: { count: number }
  summaries: {
    ['pers:interior_orientation']: [{ make: string; model: string }]
  }
  'geovisio:status': string
}

export interface CheckboxInterface {
  isChecked: boolean
  isIndeterminate?: boolean
  name: string
}

export interface SequenceInterface {
  title: string
  id: string
  pictures: any[]
  picturesOnError: UploadErrorInterface[] | []
  pictureCount: number
  pictureSize: string
}
export interface UploadErrorInterface extends UploadErrorRejectedInterface {
  name: string
}
interface UploadErrorRejectedInterface {
  message: string
  severity: string
  reason: string
  details?: { missing_fields: string[] }
}
export interface UploadErrorFilesInterface {
  content_md5: string
  file_name: string
  file_type: string
  inserted_at: string
  picture_id: string
  size: number
  rejected: UploadErrorRejectedInterface
}
export interface UploadSetCreatedInterface {
  id: string
  created_at: string
  completed: boolean
  dispatched: boolean
  account_id: string
  title: string
  estimated_nb_files: number
  sort_method: string
  split_distance: number
  split_time: number
  duplicate_distance: number
  duplicate_rotation: number
  associated_collections: AssociatedCollectionInterface[]
  nb_items: number
  links: LinkInterface[]
  ready: boolean
  items_status: ItemsStatusInterface
}

export interface AssociatedCollectionInterface {
  id: string
  nb_items: number
  extent: {
    temporal: {
      interval: [string]
    }
  }
  title: string
  items_status: ItemsStatusInterface
  links: LinkInterface[]
  ready: boolean
}

export interface FolderInterface {
  folderName: string
  files: File[]
}

interface LinkInterface {
  rel: string
  type: string
  href: string
}

interface ItemsStatusInterface {
  prepared: number
  preparing: number
  rejected: number
  broken: number
  not_processed: number
}

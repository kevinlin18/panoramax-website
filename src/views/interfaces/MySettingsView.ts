interface ResponseUsersMeTokensIdInterface {
  description: string
  generated_at: string
  id: string
  jwt_token: string
}

export interface LinkInterface {
  href: string
  rel: string
  type: string
}

export interface ResponseUsersMeTokensInterface {
  description: string
  generated_at: string
  id: string
  isLoading?: boolean
  isHidden?: boolean
  copied?: boolean
  links: [LinkInterface]
  token?: ResponseUsersMeTokensIdInterface
}

export interface SequenceLinkInterface {
  id: string
  href: string
  rel: string
  title: string
  type: string
  created: Date
  extent: { temporal: { interval: [Date[]] }; spatial: { bbox: [number[]] } }
  ['stats:items']: { count: number }
  ['geovisio:status']: string
  ['geovisio:length_km']: number
}

export interface PositionInterface {
  bottom: number
  top: number
  right: number
  left: number
  y: number
  x: number
}

export interface DateInterface {
  end: string | null
  start: string | null
  type: string
}

export interface StandAloneInterface extends ParamsViewerInterface {
  maxZoom: number
  minZoom: number
  padding: { top: number; bottom: number; left: number; right: number }
  speed: number
  zoom: number
  users?: string[]
}
export interface EditorInterface extends ParamsViewerInterface {
  users?: string[]
  selectedSequence?: string
  minZoom: number
}
export interface ViewerInterface {
  widgets: { customWidget: HTMLDivElement; mapAttribution?: string }
  map: {
    startWide: boolean
    center?: number[]
    hash?: boolean
    maxZoom?: number
    zoom?: number
    raster?:
      | {
          type: string
          tiles: string[]
          tileSize: number
        }
      | string
  }
  fetchOptions?: { credentials?: string }
}

export interface ParamsViewerInterface {
  lang: string
  style?: void
  selectedPicture?: string
  fetchOptions?: { credentials?: string }
  map?: {
    raster?:
      | {
          type: string
          tiles: string[]
          tileSize: number
        }
      | string
  }
}

export interface ParamsEditorStandaloneInterface {
  lang: string
  style?: void
  selectedPicture?: string
  fetchOptions?: { credentials?: string }
  raster?:
    | {
        type: string
        tiles: string[]
        tileSize: number
      }
    | string
}

import type {
  ResponseUserPhotoInterface,
  ResponseUserPhotoLinksInterface
} from '@/views/interfaces/MySequenceView'

function imageStatus(imageStatus: string, sequenceStatus: string): string {
  if (sequenceStatus === 'hidden') return sequenceStatus
  return imageStatus
}
function scrollIntoSelected(id: string, userPhotos: string[]): void {
  const itemPosition = userPhotos.map((el: string) => el).indexOf(id)
  const elementTarget = document.querySelector(`#el-list${itemPosition}`)
  if (elementTarget) elementTarget.scrollIntoView({ behavior: 'smooth' })
}

function photoToDeleteOrPatchSelected(
  item: ResponseUserPhotoInterface,
  imagesToDelete: string[]
): boolean {
  return imagesToDelete.includes(item.id)
}

function spliceIntoChunks(arr: string[], chunkSize: number) {
  const res = []
  arr = ([] as string[]).concat(...arr)
  while (arr.length) {
    res.push(arr.splice(0, chunkSize))
  }
  return res
}

function formatPaginationItems(
  items: [ResponseUserPhotoLinksInterface]
): ResponseUserPhotoLinksInterface[] {
  const filterItems = items.filter(
    (el: ResponseUserPhotoLinksInterface) =>
      el.rel === 'first' ||
      el.rel === 'last' ||
      el.rel === 'next' ||
      el.rel === 'prev'
  )
  return filterItems.map((el: ResponseUserPhotoLinksInterface) => {
    if (el.rel === 'first') return { ...el, rel: 'double-left' }
    if (el.rel === 'last') return { ...el, rel: 'double-right' }
    if (el.rel === 'next') return { ...el, rel: 'right' }
    if (el.rel === 'prev') return { ...el, rel: 'left' }
    return el
  })
}
function formatPagination(
  paginationLinks: ResponseUserPhotoLinksInterface[]
): ResponseUserPhotoLinksInterface[] | [] {
  let lk:
    | [...ResponseUserPhotoLinksInterface[], object, object]
    | [object, object, ...ResponseUserPhotoLinksInterface[]]
    | [] = []
  paginationLinks.map((e: ResponseUserPhotoLinksInterface) => {
    if (e?.rel === 'left') return (lk = [...paginationLinks, {}, {}])
    if (e?.rel === 'right') return (lk = [{}, {}, ...paginationLinks])
  })
  return lk
}
export {
  imageStatus,
  scrollIntoSelected,
  photoToDeleteOrPatchSelected,
  spliceIntoChunks,
  formatPagination,
  formatPaginationItems
}

import axios from 'axios'
import type { UploadSetCreatedInterface } from '@/views/interfaces/UploadPicturesView'

interface PictureCreatedInterface {
  data: {
    assets: object
    bbox: number[]
    collection: string
    geometry: object
    id: string
    links: object[]
    properties: object
    providers: object[]
    stac_extensions: string[]
    stac_version: string
    type: string
  }
}

function createUploadSets(params: {
  title: string
  estimated_nb_files: number
  metadata?: [{ folderName: string; files: number }]
}): Promise<{ data: UploadSetCreatedInterface }> {
  return axios.post('api/upload_sets', params)
}

async function createAPictureToAnUploadSet(
  id: string,
  body: FormData
): Promise<PictureCreatedInterface> {
  const { data } = await axios.post(`api/upload_sets/${id}/files`, body)
  return data
}
async function fetchUploadSetFiles(uploadSetId: string) {
  return await axios.get(`api/upload_sets/${uploadSetId}/files`)
}
async function completeUploadSet(uploadSetId: string) {
  return await axios.post(`api/upload_sets/${uploadSetId}/complete`)
}

function fetchUploadSet(uploadSetId: string): Promise<{
  data: UploadSetCreatedInterface
}> {
  return axios.get(`api/upload_sets/${uploadSetId}`)
}

function fetchUploadSets(params: string): Promise<{
  data: { upload_sets: UploadSetCreatedInterface[] }
}> {
  return axios.get(
    `api/users/me/upload_sets?filter=${encodeURIComponent(params)}`
  )
}

export {
  createUploadSets,
  createAPictureToAnUploadSet,
  fetchUploadSetFiles,
  fetchUploadSet,
  fetchUploadSets,
  completeUploadSet
}

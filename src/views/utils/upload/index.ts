import type { FolderInterface } from '@/views/interfaces/UploadPicturesView'
import { formatDate } from '@/utils/dates'
import { manageSlashUrl } from '@/utils'

function sortByName(fileList: File[]): File[] {
  return fileList.sort((a: File, b: File) =>
    a.name.localeCompare(b.name, navigator.languages[0] || navigator.language, {
      numeric: true,
      ignorePunctuation: true
    })
  )
}
function isFolder(
  value: File[] | FileList | FolderInterface[]
): value is FolderInterface[] {
  return value.length > 0 && 'folderName' in value[0]
}
function checkFileExtension(files: File[]) {
  return files.filter((file: File) => {
    if (file.type?.length > 0) {
      return file.type === 'image/jpeg' || file.type === 'image/jpg'
    }
    if (file.name?.length > 0) {
      const extension = file.name.split('.').pop()?.toLowerCase()
      return extension === 'jpeg' || extension === 'jpg'
    }
    return file
  })
}
function countTotalFiles(allFiles: FolderInterface[]) {
  return allFiles.reduce(
    (total: number, folder: FolderInterface) => total + folder.files.length,
    0
  )
}
function calcPercentage(value: number, totalValue: number): number {
  if (value && totalValue) return Math.floor((value / totalValue) * 100)
  return 0
}
function conditionalStatusNumbers(value: number | undefined): number {
  return value ? value : 0
}
function formatSequenceTitle(value: string): string {
  return `${value}${formatDate(new Date(), 'LL')}, ${formatDate(
    new Date(),
    'HH:mm:ss'
  )}`
}
function formatUrl(id: string) {
  return `${manageSlashUrl()}api/collections/${id}/thumb.jpg`
}
export {
  sortByName,
  isFolder,
  checkFileExtension,
  countTotalFiles,
  calcPercentage,
  conditionalStatusNumbers,
  formatSequenceTitle,
  formatUrl
}

import { it, describe, expect } from 'vitest'
import {
  createLink,
  createFullScreenButton
} from '@/components-viewer/reportLink'

describe('Utils Functions', () => {
  it('createLink should return the correct HTML string', () => {
    const href = '/test'
    const text = 'Test Link'
    const result = createLink(href, text)
    expect(result).toBe(
      `<a href='mailto:signalement.ign@panoramax.fr${href}' target='_blank' title='${text}' class='gvs-btn gvs-widget-bg gvs-btn-large' style='font-size: 1.6em;display: block; margin-top: 0.5em;'><i class="bi bi-exclamation-triangle"></i></a>`
    )
  })

  it('createFullScreenButton should return the correct HTML string', () => {
    const result = createFullScreenButton()
    expect(result).toBe(`<button type='button' onClick="
                                  const header = document.getElementById('navHeader')
                                  const footer = document.getElementById('navFooter')
                                  const icon = document.getElementById('iconScreen')
                                  const home = document.getElementById('homePage')
                                  if (header) {
                                    const isHiddenHeader = header.classList.contains('hidden')
                                    if (isHiddenHeader) header.classList.remove('hidden')
                                    else header.classList.add('hidden')
                                  }
                                  if (footer) {
                                    const isHiddenHeader = footer.classList.contains('hidden')
                                    if (isHiddenHeader) footer.classList.remove('hidden')
                                    else footer.classList.add('hidden')
                                  }
                                  if (icon) {
                                    const isIconNotFull = icon.classList.contains('bi-fullscreen')
                                    if(isIconNotFull) {
                                      icon.classList.remove('bi-fullscreen')
                                      icon.classList.add('bi-fullscreen-exit') 
                                    } else {
                                      icon.classList.remove('bi-fullscreen-exit')
                                      icon.classList.add('bi-fullscreen')
                                    }
                                  }
                                  if (home) {
                                    const isHomeFull = home.classList.contains('full-viewer')
                                    if(isHomeFull) home.classList.remove('full-viewer')
                                    else home.classList.add('full-viewer')
                                  }
                                  "
                                  class='gvs-btn gvs-widget-bg gvs-btn-large'
          >
            <i id='iconScreen' class="bi bi-fullscreen" style='height: 100%;'></i>
          </button>`)
  })
})

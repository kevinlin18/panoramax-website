import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Legal from '@/components/legal/Legal.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Legal, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.htmlContent).toBe('')
    })
    describe('When the props are filled', () => {
      it('should render the component with the html content', () => {
        const wrapper = shallowMount(Legal, {
          global: {
            plugins: [i18n]
          },
          props: {
            htmlContent: '<p>My content</p>'
          }
        })
        expect(wrapper.vm.htmlContent).toBe('<p>My content</p>')
        expect(wrapper.html()).contains('<p>My content</p>')
      })
    })
  })
})

import { it, describe, expect, vi } from 'vitest'
import { flushPromises, shallowMount } from '@vue/test-utils'
import Summary from '@/components/legal/Summary.vue'
import i18n from '@/tests/unit/config'
import axios from 'axios'

const mockResponseLegalPage = {
  data: {
    languages: ['fr', 'en']
  }
}
const mockFoundPage = {
  data: '<p>Legal summary content</p>'
}
describe('Template', () => {
  describe('When the languages page are fetched', () => {
    it('should render the page with the content', async () => {
      vi.spyOn(axios, 'get')
        .mockResolvedValueOnce(mockResponseLegalPage)
        .mockResolvedValueOnce(mockFoundPage)
      const wrapper = shallowMount(Summary, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      await flushPromises()
      expect(axios.get).toHaveBeenCalledWith(
        'api/pages/end-user-agreement-license-summary'
      )
      expect(wrapper.vm.contentSummary).toEqual(mockFoundPage.data)
    })
  })
})

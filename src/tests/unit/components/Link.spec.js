import { it, describe, vi, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Link from '@/components/Link.vue'
import * as img from '@/utils/image'

vi.mock('vue-i18n', () => ({
  useI18n: () => ({
    t: (key) => key
  })
}))
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => vi.fn()
}))
const stubs = {
  'router-link': {
    template: '<router-link to=""></router-link>'
  }
}
describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Link, {
        global: {}
      })

      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.route).toStrictEqual({})
      expect(wrapper.vm.pathExternal).toBe('')
      expect(wrapper.vm.look).toBe('')
      expect(wrapper.vm.type).toBe(null)
      expect(wrapper.vm.alt).toBe('')
      expect(wrapper.vm.icon).toBe(null)
      expect(wrapper.vm.target).toBe(null)
      expect(wrapper.vm.image).toBe(null)
      expect(wrapper.vm.disabled).toBe(false)
    })
  })
  describe('When the component is an external link', () => {
    it('should render the component as a <a>', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs
        },
        props: {
          type: 'external'
        }
      })
      expect(wrapper.html()).contains('<a')
    })
    it('should render an icon inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs
        },
        props: {
          type: 'external',
          icon: 'my-icon'
        }
      })
      expect(wrapper.html()).contains('my-icon')
    })
    it('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs
        },
        props: {
          type: 'external',
          text: 'my-text'
        }
      })
      expect(wrapper.html()).contains('my-text')
    })
    it('should render an image inside the link', () => {
      const imgSrc = 'my-url.png'
      const imgPath = vi.spyOn(img, 'img')
      imgPath.mockReturnValue(imgSrc)
      const wrapper = shallowMount(Link, {
        global: {
          stubs
        },
        props: {
          type: 'external',
          image: {
            url: imgSrc,
            alt: 'my-alt'
          }
        }
      })
      expect(wrapper.html()).contains('<img')
      expect(wrapper.html()).contains(imgSrc)
      expect(wrapper.html()).contains('my-alt')
    })
    it('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs
        },
        props: {
          type: 'external',
          disabled: true
        }
      })
      expect(wrapper.html()).contains('disabled')
    })
  })

  describe('When the component is an internal link', () => {
    it('should render the component as an internal link', () => {
      const wrapper = shallowMount(Link, {
        global: {}
      })
      expect(wrapper.html()).contains('<router-link')
    })
    it('should render an icon inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {},
        props: {
          type: 'internal',
          icon: 'my-icon'
        }
      })
      expect(wrapper.html()).contains('my-icon')
    })
    it('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {},
        props: {
          text: 'my-text'
        }
      })
      expect(wrapper.html()).contains('my-text')
    })
    it('should render the disabled attribute', () => {
      const wrapper = shallowMount(Link, {
        global: {},
        props: {
          disabled: true
        }
      })
      expect(wrapper.html()).contains('disabled')
    })
  })
})

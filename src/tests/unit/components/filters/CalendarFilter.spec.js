import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import CalendarFilter from '@/components/filters/CalendarFilter.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(CalendarFilter, {
        global: {
          plugins: [i18n],
          stubs: {
            'v-date-picker': true
          }
        }
      })
      expect(wrapper.vm.type).toBe('')
      expect(wrapper.vm.rangeSelected).toStrictEqual({
        start: null,
        end: null,
        type: ''
      })
    })
    describe('When the props are filled', () => {
      it('should render the component with the defaultText', () => {
        const wrapper = shallowMount(CalendarFilter, {
          global: {
            plugins: [i18n],
            stubs: {
              'v-date-picker': true
            }
          },
          props: {
            type: 'datetime',
            rangeSelected: {
              start: 'Thu Oct 21 2024 08:12:46 GMT+0000 (Greenwich Mean Time)',
              end: 'Thu Oct 31 2024 08:12:46 GMT+0000 (Greenwich Mean Time)',
              type: 'created'
            }
          }
        })
        expect(wrapper.html()).contains('value="2024-10-21"')
        expect(wrapper.html()).contains('value="2024-10-31"')
        expect(wrapper.html()).contains('text="Fermer"')
        expect(wrapper.html()).contains('text="Réinitialiser"')
      })
    })
  })
  describe('Event', () => {
    describe('When the reset button is clicked', () => {
      it('should clear the dates and trigger', async () => {
        const wrapper = shallowMount(CalendarFilter, {
          global: {
            plugins: [i18n],
            stubs: {
              'v-date-picker': true
            }
          },
          props: {
            type: 'datetime',
            rangeSelected: {
              start: 'Thu Oct 21 2024 08:12:46 GMT+0000 (Greenwich Mean Time)',
              end: 'Thu Oct 31 2024 08:12:46 GMT+0000 (Greenwich Mean Time)',
              type: 'created'
            }
          }
        })

        const buttonReset = wrapper.find('[data-test="button-reset-calendar"]')
        expect(buttonReset.exists()).toBe(true)
        await buttonReset.trigger('trigger')
        expect(wrapper.emitted()).toHaveProperty('triggerDate')
        expect(wrapper.emitted().triggerDate[0][0]).toEqual({
          start: null,
          end: null,
          type: 'datetime'
        })
        expect(wrapper.html()).not.toContain('value="2024-10-21"')
        expect(wrapper.html()).not.toContain('value="2024-10-31"')
      })
    })
    describe('When the input calendar is filled', () => {
      it('should update and trigger the values', async () => {
        const wrapper = shallowMount(CalendarFilter, {
          global: {
            plugins: [i18n],
            stubs: {
              'v-date-picker': true
            }
          },
          props: {
            type: 'datetime'
          }
        })
        const inputStart = wrapper.find('[data-test="input-start-calendar"]')
        const inputEnd = wrapper.find('[data-test="input-end-calendar"]')
        expect(inputStart.exists()).toBe(true)
        expect(inputEnd.exists()).toBe(true)
        await inputStart.setValue('2024-10-05')
        await inputEnd.setValue('2024-10-11')
        expect(wrapper.html()).contains('value="2024-10-05"')
        expect(wrapper.html()).contains('value="2024-10-11"')
        expect(wrapper.emitted()).toHaveProperty('triggerDate')
        expect(wrapper.emitted().triggerDate[0][0]).toEqual({
          start: '2024-10-05 12:00 AM',
          end: '2024-10-11 11:59 PM',
          type: 'datetime'
        })
      })
    })
  })
})

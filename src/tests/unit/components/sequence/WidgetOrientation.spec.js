import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import WidgetOrientation from '@/components/sequence/WidgetOrientation.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(WidgetOrientation, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.roadDegrees).toBe(0)
      expect(wrapper.vm.seqBruteDeg).toBe(0)
    })
    describe('When the component have props filled', () => {
      it('should render the component all the element', () => {
        const wrapper = shallowMount(WidgetOrientation, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            roadDegrees: 45,
            seqBruteDeg: 22
          }
        })
        expect(wrapper.html()).contains(
          'class="wrapper-img-road" style="transform: rotate(45deg);"'
        )
        expect(wrapper.html()).contains(
          'src="/assets/images/car.svg" alt="" style="transform: rotate(45deg);" class="car-img"'
        )
        expect(wrapper.html()).contains(
          'style="transform: rotate(22deg);" id="rotateWrapper" class="rotate-wrapper"'
        )
        expect(wrapper.html()).contains(
          'src="/assets/images/icon/cursor-arrow.svg"'
        )
        expect(wrapper.html()).contains('class="arrow-img arrow-img-1"')
        expect(wrapper.html()).contains('class="arrow-img arrow-img-2"')
      })
    })
  })
  // TODO TEST -> All Events
})

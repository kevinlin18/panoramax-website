import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import ImageThumb from '@/components/sequence/ImageThumb.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(ImageThumb, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.count).toBe(0)
      expect(wrapper.vm.href).toBe('')
    })
    describe('When the props are filled', () => {
      it('should render the component with information', () => {
        const wrapper = shallowMount(ImageThumb, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            count: 3,
            href: 'https://www.saikle.fr'
          }
        })
        expect(wrapper.html()).contains('src="https://www.saikle.fr"')
      })
    })
  })
})

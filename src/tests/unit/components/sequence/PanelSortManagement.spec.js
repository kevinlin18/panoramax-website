import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import PanelSortManagement from '@/components/sequence/PanelSortManagement.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(PanelSortManagement, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.sequenceSorted).toBe(null)
    })
    describe('When the component have props filled', () => {
      it('should render the component all the element', () => {
        const wrapper = shallowMount(PanelSortManagement, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        expect(wrapper.html()).contains('pages.sequence.sort_panel_title')
        expect(wrapper.html()).contains('pages.sequence.sort_panel_settings')
        expect(wrapper.html()).contains(
          'name="sort" id="gpsdate" label="pages.sequence.sort_panel_check_gps" value=""'
        )
        expect(wrapper.html()).contains(
          'name="sort" id="filedate" label="pages.sequence.sort_panel_check_file" value=""'
        )
        expect(wrapper.html()).contains(
          'name="sort" id="filename" label="pages.sequence.sort_panel_check_name" value=""'
        )
        expect(wrapper.html()).contains(
          'pages.sequence.sort_panel_settings_order'
        )
        expect(wrapper.html()).contains('increased="true"></input-switch')
      })
      it('should render the component with gpsdate selected', () => {
        const wrapper = shallowMount(PanelSortManagement, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            sequenceSorted: '-gpsdate'
          }
        })
        expect(wrapper.html()).contains(
          'name="sort" id="gpsdate" label="pages.sequence.sort_panel_check_gps" value="gpsdate"'
        )
        expect(wrapper.html()).contains('increased="false"></input-switch')
      })
    })
  })
  describe('Emit functions', () => {
    it('should emit triggerSort event', async () => {
      const wrapper = shallowMount(PanelSortManagement, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          sequenceSorted: '-gpsdate'
        }
      })
      await wrapper.vm.triggerSort('-gpsdate')
      expect(wrapper.emitted('triggerSort')).toHaveLength(1)
      expect(wrapper.emitted('triggerSort')[0]).toEqual(['-gpsdate'])
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import DateInterval from '@/components/sequence/DateInterval.vue'
import i18n from '@/tests/unit/config'
describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(DateInterval, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.firstDate).toBe(null)
      expect(wrapper.vm.lastDate).toBe(null)
    })
    describe('When the props are filled', () => {
      it('should render the component with the dates', () => {
        const wrapper = shallowMount(DateInterval, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            firstDate: new Date('2024-10-10T11:53:10Z'),
            lastDate: new Date('2024-10-30T12:53:10Z')
          }
        })
        expect(wrapper.html()).contains('10 Oct 2024 - 30 Oct 2024')
        expect(wrapper.html()).contains('11:53')
        expect(wrapper.html()).contains('481h0')
        expect(wrapper.html()).contains('12:53')
      })
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import PanelPhotosManagement from '@/components/sequence/PanelPhotosManagement.vue'
import i18n from '@/tests/unit/config'
import { createPinia } from 'pinia'
const pinia = createPinia()

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(PanelPhotosManagement, {
        global: {
          plugins: [i18n, pinia],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.isSequenceOwner).toBe(false)
      expect(wrapper.vm.imagesSelectedHaveDifferentStatus).toBe(false)
      expect(wrapper.vm.itemSelected).toBe('')
      expect(wrapper.vm.menuHeight).toBe('0')
      expect(wrapper.vm.fullImagesToDelete).toStrictEqual([])
      expect(wrapper.vm.selfLink).toStrictEqual([])
      expect(wrapper.vm.paginationLinks).toStrictEqual([])
      expect(wrapper.vm.pictures).toStrictEqual([])
      expect(wrapper.vm.picturesToDelete).toStrictEqual([])
      expect(wrapper.vm.sequence).toStrictEqual({})
    })
    describe('When the component have props filled', () => {
      it('should render the component with images selected', () => {
        const pictures = [
          {
            assets: { thumb: { href: 'thumbHref' }, hd: { href: 'hdHref' } },
            properties: { datetime: new Date(), 'geovisio:status': 'ready' },
            id: 'pictureId',
            bbox: [1, 2, 3, 4]
          }
        ]
        const sequence = [
          {
            id: 'seqId',
            title: 'title',
            description: 'descr',
            license: 'license',
            created: new Date(),
            taken: 'taken date',
            location: 'location',
            imageCount: 4,
            duration: 'duration',
            camera: 'camera',
            cameraModel: 'camera model',
            status: 'ready',
            providers: [{ name: 'provider', roles: ['role1'] }],
            extent: {
              temporal: { interval: ['date'] },
              spatial: { bbox: ['1', '2'] }
            }
          }
        ]
        const paginationLinks = [
          {
            href: 'href',
            rel: 'rel',
            title: 'title',
            type: 'type'
          }
        ]
        const selfLink = [
          {
            href: 'href',
            rel: 'self',
            title: 'title',
            type: 'type'
          }
        ]
        const fullImagesToDelete = [
          {
            assets: { thumb: { href: 'thumbHref' }, hd: { href: 'hdHref' } },
            properties: { datetime: new Date(), 'geovisio:status': 'ready' },
            id: 'pictureId',
            bbox: [1, 2, 3, 4]
          }
        ]
        const wrapper = shallowMount(PanelPhotosManagement, {
          global: {
            plugins: [i18n, pinia],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isSequenceOwner: true,
            imagesSelectedHaveDifferentStatus: true,
            itemSelected: 'pictureId',
            menuHeight: '10px',
            pictures,
            picturesToDelete: ['1', '2'],
            sequence,
            paginationLinks,
            selfLink,
            fullImagesToDelete
          }
        })
        expect(wrapper.html()).contains('<image-item')
        expect(wrapper.html()).contains('selectedonmap="true"')
        expect(wrapper.html()).contains('href="thumbHref"')
        expect(wrapper.html()).contains('hrefhd="hdHref"')
        expect(wrapper.html()).contains('ischecked="false"')
        expect(wrapper.html()).contains('isindeterminate="true"')
        expect(wrapper.html()).contains('pages.sequence.picture_selected')
        expect(wrapper.html()).contains(
          'icon="bi bi-eye" disabled="false" isloading="false"'
        )
        expect(wrapper.html()).contains(
          'icon="bi bi-trash" disabled="false" isloading="false"'
        )
        expect(wrapper.html()).contains('pages.sequence.hide_photo_tooltip')
        expect(wrapper.html()).contains('pages.sequence.delete_photo_tooltip')
        expect(wrapper.html()).contains('<pagination')
        expect(wrapper.html()).contains('selflink="')
      })
      it('should render the component with no images', () => {
        const pictures = []
        const sequence = [
          {
            id: 'seqId',
            title: 'title',
            description: 'descr',
            license: 'license',
            created: new Date(),
            taken: 'taken date',
            location: 'location',
            imageCount: 4,
            duration: 'duration',
            camera: 'camera',
            cameraModel: 'camera model',
            status: 'ready',
            providers: [{ name: 'provider', roles: ['role1'] }],
            extent: {
              temporal: { interval: ['date'] },
              spatial: { bbox: ['1', '2'] }
            }
          }
        ]
        const paginationLinks = []
        const selfLink = [
          {
            href: 'href',
            rel: 'self',
            title: 'title',
            type: 'type'
          }
        ]
        const fullImagesToDelete = []
        const wrapper = shallowMount(PanelPhotosManagement, {
          global: {
            plugins: [i18n, pinia],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isSequenceOwner: true,
            imagesSelectedHaveDifferentStatus: false,
            itemSelected: '',
            menuHeight: '10px',
            pictures,
            picturesToDelete: [],
            sequence,
            paginationLinks,
            selfLink,
            fullImagesToDelete
          }
        })
        expect(wrapper.html()).contains('class="no-photo"')
        expect(wrapper.html()).contains('pages.sequence.no_image')
      })
      it('should render the component with all the images selected', () => {
        const pictures = [
          {
            assets: { thumb: { href: 'thumbHref' }, hd: { href: 'hdHref' } },
            properties: { datetime: new Date(), 'geovisio:status': 'ready' },
            id: 'pictureId',
            bbox: [1, 2, 3, 4]
          }
        ]
        const sequence = [
          {
            id: 'seqId',
            title: 'title',
            description: 'descr',
            license: 'license',
            created: new Date(),
            taken: 'taken date',
            location: 'location',
            imageCount: 4,
            duration: 'duration',
            camera: 'camera',
            cameraModel: 'camera model',
            status: 'ready',
            providers: [{ name: 'provider', roles: ['role1'] }],
            extent: {
              temporal: { interval: ['date'] },
              spatial: { bbox: ['1', '2'] }
            }
          }
        ]
        const wrapper = shallowMount(PanelPhotosManagement, {
          global: {
            plugins: [i18n, pinia],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isSequenceOwner: true,
            pictures,
            picturesToDelete: [1],
            sequence
          }
        })
        expect(wrapper.html()).contains(
          'ischecked="true" isindeterminate="false"></input'
        )
      })
      it('should render the component with images hidden', () => {
        const pictures = [
          {
            assets: { thumb: { href: 'thumbHref' }, hd: { href: 'hdHref' } },
            properties: { datetime: new Date(), 'geovisio:status': 'hidden' },
            id: 'pictureId',
            bbox: [1, 2, 3, 4]
          }
        ]
        const sequence = [
          {
            id: 'seqId',
            title: 'title',
            description: 'descr',
            license: 'license',
            created: new Date(),
            taken: 'taken date',
            location: 'location',
            imageCount: 4,
            duration: 'duration',
            camera: 'camera',
            cameraModel: 'camera model',
            status: 'ready',
            providers: [{ name: 'provider', roles: ['role1'] }],
            extent: {
              temporal: { interval: ['date'] },
              spatial: { bbox: ['1', '2'] }
            }
          }
        ]
        const wrapper = shallowMount(PanelPhotosManagement, {
          global: {
            plugins: [i18n, pinia],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isSequenceOwner: true,
            pictures,
            picturesToDelete: [],
            sequence
          }
        })
        expect(wrapper.html()).contains(
          'status="hidden" selected="false"></image-item'
        )
      })
    })
  })
  describe('Emit functions', () => {
    it('should emit triggerInputCheck event', async () => {
      const wrapper = shallowMount(PanelPhotosManagement, {
        global: {
          plugins: [i18n, pinia],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      await wrapper.vm.triggerInputCheck({
        isChecked: true,
        isIndeterminate: false
      })
      expect(wrapper.emitted('triggerInputCheck')).toHaveLength(1)
      expect(wrapper.emitted('triggerInputCheck')[0]).toEqual([
        { isChecked: true, isIndeterminate: false }
      ])
    })
    it('should emit triggerGoToNextPage event', async () => {
      const wrapper = shallowMount(PanelPhotosManagement, {
        global: {
          plugins: [i18n, pinia],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      await wrapper.vm.triggerGoToNextPage('nextPage')
      expect(wrapper.emitted('triggerGoToNextPage')).toHaveLength(1)
      expect(wrapper.emitted('triggerGoToNextPage')[0]).toEqual(['nextPage'])
    })

    it('should emit triggerSelectImageAndMove event', async () => {
      const wrapper = shallowMount(PanelPhotosManagement, {
        global: {
          plugins: [i18n, pinia],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      const picture = {
        assets: { thumb: { href: 'thumbHref' }, hd: { href: 'hdHref' } },
        properties: { datetime: new Date(), 'geovisio:status': 'ready' },
        id: 'pictureId',
        bbox: [1, 2, 3, 4]
      }
      await wrapper.vm.triggerSelectImageAndMove(picture)
      expect(wrapper.emitted('triggerSelectImageAndMove')).toHaveLength(1)
      expect(wrapper.emitted('triggerSelectImageAndMove')[0]).toEqual([picture])
    })

    it('should emit triggerPatchOrDeleteCollectionItems event', async () => {
      const wrapper = shallowMount(PanelPhotosManagement, {
        global: {
          plugins: [i18n, pinia],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      await wrapper.vm.triggerPatchOrDeleteCollectionItems('itemToDelete')
      expect(
        wrapper.emitted('triggerPatchOrDeleteCollectionItems')
      ).toHaveLength(1)
      expect(wrapper.emitted('triggerPatchOrDeleteCollectionItems')[0]).toEqual(
        ['itemToDelete']
      )
    })
  })
})

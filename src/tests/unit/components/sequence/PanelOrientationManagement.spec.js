import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import PanelOrientationManagement from '@/components/sequence/PanelOrientationManagement.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(PanelOrientationManagement, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.roadDegrees).toBe(0)
      expect(wrapper.vm.seqBruteDeg).toBe(0)
    })
    describe('When the component have props filled', () => {
      it('should render the component all the element', () => {
        const wrapper = shallowMount(PanelOrientationManagement, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            roadDegrees: 45,
            seqBruteDeg: 22
          }
        })
        expect(wrapper.html()).contains(
          'pages.sequence.orientation_panel_title'
        )
        expect(wrapper.html()).contains(
          'pages.sequence.orientation_input_label'
        )
        expect(wrapper.html()).contains(
          'pages.sequence.orientation_input_placeholder'
        )
        expect(wrapper.html()).contains('<widget-orientation')
        expect(wrapper.html()).contains('<information-card')
        expect(wrapper.html()).contains('<input')
        expect(wrapper.html()).contains('<button')
        expect(wrapper.html()).contains(
          'type="number" min="-180" max="180" text="-23"'
        )
      })
    })
    describe('Emit functions', () => {
      it('should emit triggerAngle event', async () => {
        const wrapper = shallowMount(PanelOrientationManagement, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            roadDegrees: 45,
            seqBruteDeg: 22
          }
        })
        const valueToEmit = 22 - 45
        await wrapper.vm.triggerAngle(valueToEmit)
        expect(wrapper.emitted('triggerAngle')).toHaveLength(1)
        expect(wrapper.emitted('triggerAngle')[0]).toEqual([valueToEmit])
      })
    })
  })
})

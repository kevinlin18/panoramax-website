import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Footer from '@/components/Footer.vue'
import Link from '@/components/Link.vue'
import i18n from '@/tests/unit/config'
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => vi.fn()
}))
describe('Template', () => {
  it('should have the links without the ay11 link', () => {
    const wrapper = shallowMount(Footer, {
      global: {
        stubs: { Link },
        plugins: [i18n]
      }
    })
    expect(wrapper.html()).contains('href="https://panoramax.fr/"')
    expect(wrapper.html()).contains('logo.jpeg')
    expect(wrapper.html()).contains('Découvrir Panoramax')

    expect(wrapper.html()).contains('href="https://gitlab.com/geovisio"')
    expect(wrapper.html()).contains('gitlab-logo.svg')
    expect(wrapper.html()).contains('Voir le code')
  })
  it('should have the ay11 link', () => {
    Object.create(window)
    const url = 'http://test.ign.fr'
    Object.defineProperty(window, 'location', {
      value: {
        href: url
      },
      writable: true // possibility to override
    })
    const wrapper = shallowMount(Footer, {
      global: {
        stubs: { Link },
        plugins: [i18n]
      }
    })
    expect(wrapper.html()).contains('title="Accessibilité : non conforme"')
  })
  it('should have the legals link', () => {
    const wrapper = shallowMount(Footer, {
      global: {
        stubs: { Link },
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg,
          authConf: {
            pages: ['end-user-license-agreement', 'terms-of-service']
          }
        }
      }
    })
    expect(wrapper.html()).contains('general.footer.tos')
    expect(wrapper.html()).contains('general.footer.terms-of-service')
  })
})

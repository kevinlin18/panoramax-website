import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InputCheckbox from '@/components/InputCheckbox.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.name).toBe(null)
      expect(wrapper.vm.label).toBe('')
      expect(wrapper.vm.isChecked).toBe(false)
      expect(wrapper.vm.isIndeterminate).toBe(false)
    })
  })
  describe('When the component have a label', () => {
    it('should render the component with a href, a hrefHd and a date', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          label: 'my label'
        }
      })
      expect(wrapper.html()).contains('class="label"')
      expect(wrapper.html()).contains('my label')
    })
  })
  describe('When the component is checked', () => {
    it('should render the component with the checked icon', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          isChecked: true,
          isIndeterminate: false
        }
      })
      expect(wrapper.html()).contains('class="icon bi bi-check-square"')
    })
  })
  describe('When the component is not checked', () => {
    it('should render the component with the not checked icon', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          isChecked: false,
          isIndeterminate: false
        }
      })
      expect(wrapper.html()).contains('class="icon bi bi-square"')
    })
  })
  describe('When the component is not checked', () => {
    it('should render the component with the not checked icon', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          isChecked: false,
          isIndeterminate: true
        }
      })
      expect(wrapper.html()).contains('class="icon bi bi-dash-square"')
    })
  })
  describe('When the input is trigger', () => {
    it('should emit', () => {
      const wrapper = shallowMount(InputCheckbox, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      wrapper.vm.$emit('trigger', true)
      expect(wrapper.emitted().trigger).toBeTruthy()
      expect(wrapper.emitted().trigger[0][0]).toEqual(true)
    })
  })
})

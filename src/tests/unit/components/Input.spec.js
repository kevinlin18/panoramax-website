import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Input from '@/components/Input.vue'
import i18n from '@/tests/unit/config'
describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Input, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.placeholder).toBe('')
    })
  })
  describe('When the props are filled', () => {
    it('should render the button with the placeholder', () => {
      const wrapper = shallowMount(Input, {
        global: {
          plugins: [i18n]
        },
        props: {
          placeholder: 'My placeholder'
        }
      })
      expect(wrapper.html()).contains('placeholder="My placeholder"')
    })
    it('should emit an input event with the prop value', async () => {
      const wrapper = shallowMount(Input, {
        global: {
          plugins: [i18n]
        },
        props: {
          text: 'My text'
        }
      })
      await wrapper.trigger('input')
      expect(wrapper.emitted()).toHaveProperty('input')
      expect(wrapper.emitted().input[0][0]).toEqual(wrapper.vm.text)
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InputSwitch from '@/components/InputSwitch.vue'

describe('Template', () => {
  it('should render the component with the goods wordings', () => {
    const wrapper = shallowMount(InputSwitch, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains(
      'pages.sequence.sort_panel_settings_order_increase'
    )
    expect(wrapper.html()).contains(
      'pages.sequence.sort_panel_settings_order_decrease'
    )
  })
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(InputSwitch, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('type="checkbox"')
      expect(wrapper.vm.increased).toBe(false)
    })
    describe('When the props are filled', () => {
      it('should render the component with increased true', () => {
        const wrapper = shallowMount(InputSwitch, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            increased: true
          }
        })
        expect(wrapper.vm.increased).toBe(true)
      })
    })
  })
  describe('When the input is trigger', () => {
    it('should emit true', async () => {
      const wrapper = shallowMount(InputSwitch, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      const checkbox = wrapper.find('input[type="checkbox"]')
      expect(checkbox.exists()).toBe(true)
      await checkbox.setChecked(true)
      await checkbox.trigger('input')
      expect(wrapper.emitted().trigger).toBeTruthy()
      expect(wrapper.emitted().trigger[0][0]).toEqual({
        isIncreaseChecked: true
      })
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Modal from '@/components/Modal.vue'
import { createI18n } from 'vue-i18n'
import fr from '@/locales/fr.json'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

describe('Template', () => {
  describe('Props', () => {
    it('should render the default props', async () => {
      document.body.innerHTML = '<div id="bs-modal"></div>'
      const wrapper = shallowMount(Modal, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.title).toEqual('')
    })
    it('should render the props filled', async () => {
      document.body.innerHTML = '<div id="bs-modal"></div>'
      const wrapper = shallowMount(Modal, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          title: 'My title'
        }
      })

      expect(wrapper.vm.title).toEqual('My title')
      expect(wrapper.html()).contains('My title')
    })
  })
})

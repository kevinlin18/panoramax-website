import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import License from '@/components/License.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(License, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.url).toBe(null)
    })
    describe('When the component have the props filled', () => {
      it('should render the component with an url and a text', () => {
        const wrapper = shallowMount(License, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            text: 'my text',
            url: 'my-url'
          }
        })
        expect(wrapper.vm.text).toBe('my text')
        expect(wrapper.vm.url).toStrictEqual('my-url')
        expect(wrapper.html()).contains(
          'pages.share_pictures.information_text2'
        )
        expect(wrapper.html()).contains('text="my text"')
        expect(wrapper.html()).contains('href="my-url"')
      })
    })
  })
})

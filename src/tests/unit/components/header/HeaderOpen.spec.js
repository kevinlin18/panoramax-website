import { vi, it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import HeaderOpen from '@/components/header/HeaderOpen.vue'
import Link from '@/components/Link.vue'
import i18n from '@/tests/unit/config'
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => vi.fn()
}))
import.meta.env.VITE_API_URL = 'api-url/'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(HeaderOpen, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.menuIsClosed).toBe(true)
      expect(wrapper.vm.userName).toBe('')
    })
    describe('When all the props are filled', () => {
      it('should render the entries Link', () => {
        const wrapper = shallowMount(HeaderOpen, {
          global: {
            plugins: [i18n],
            stubs: { Link: false }
          },
          props: {
            userProfileUrlLength: 2,
            menuIsClosed: false,
            userName: 'name'
          }
        })
        const routes = ['my-sequences', 'my-information', 'my-stats']

        const linkComponents = wrapper.findAllComponents(Link)
        const routeNames = linkComponents.map((link) => link.props().route.name)
        routes.forEach((route) => {
          expect(routeNames).toContain(route)
        })
        expect(wrapper.html()).contains('Mes photos')
        expect(wrapper.html()).contains('Mon compte')
        expect(wrapper.html()).contains('Mes stats')
        expect(wrapper.html()).contains('Déconnexion')
        expect(wrapper.html()).contains('href="api-url/api/auth/logout')
      })
    })
  })
})

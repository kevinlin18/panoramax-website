import { vi, it, describe, expect } from 'vitest'
import { shallowMount, mount } from '@vue/test-utils'
import { createI18n } from 'vue-i18n'
import { useCookies } from 'vue3-cookies'
import fr from '@/locales/fr.json'
import Burger from '@/components/header/Burger.vue'
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => vi.fn()
}))
vi.mock('vue3-cookies', () => {
  const mockCookies = {
    get: vi.fn()
  }
  return {
    useCookies: () => ({
      cookies: mockCookies
    })
  }
})
const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

describe('Template', () => {
  describe('When the user is not logged', () => {
    it('should render the component with the desktop entries', async () => {
      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = mount(Burger, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains(
        'src="file:///assets/images/logo.jpeg" alt="general.header.alt_logo" class="logo">'
      )
      expect(wrapper.html()).contains(' class="wrapper-instance">')
      expect(wrapper.html()).contains('general.header.title')
      expect(wrapper.html()).contains(
        'type="button" class="no-text-blue-dark default">'
      )
      expect(wrapper.html()).contains('general.header.contribute_text')
      expect(wrapper.html()).contains('general.header.viewer')
      expect(wrapper.html()).contains('general.footer.panoramax_site')
      expect(wrapper.html()).contains('href="https://panoramax.fr/"')
      expect(wrapper.html()).contains('general.footer.information_gitlab')
      expect(wrapper.html()).contains('href="https://gitlab.com/geovisio"')
      expect(wrapper.html()).contains('class="dropdown">')
      expect(wrapper.html()).contains('class="name">FR</span>')
    })
  })
  describe('When the user is logged', () => {
    it('should render the component with good wording keys', async () => {
      vi.spyOn(useCookies().cookies, 'get').mockReturnValue(
        '.eJw9y0EKgzAQQNG7zLoDJpmYxMuUySRDra0pooUi3r3SRZcf_tuBRdo2rzDsMBYYgFxRytljkeyQrK0YVT16m6OhUlIihgvM_Kznfa88n9V4W2_XnzcuiqgmDBQMUtYec00WO3XqAndd7OUvXkt7j6Uup5vqRx6NJziOL8SPLNU.ZVy19Q.4DkVxu-LSF11uREkn6YIwHbn_0U'
      )

      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = shallowMount(Burger, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('look="no-text-blue-dark" type="button">')
      expect(wrapper.html()).contains('general.header.contribute_text')
      expect(wrapper.html()).contains('general.header.viewer')
      expect(wrapper.html()).contains('general.footer.panoramax_site')
      expect(wrapper.html()).contains('pathexternal="https://panoramax.fr/"')
      expect(wrapper.html()).contains('general.footer.information_gitlab')
      expect(wrapper.html()).contains(
        'pathexternal="https://gitlab.com/geovisio"'
      )
      expect(wrapper.html()).contains('general.header.upload_text')
      expect(wrapper.html()).contains('general.header.sequences_text')
      expect(wrapper.html()).contains('general.header.my_information_text')
      expect(wrapper.html()).contains('general.header.my_stats_text')
      expect(wrapper.html()).contains('general.header.logout_text')
    })
  })
})

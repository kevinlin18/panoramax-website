import { it, describe, vi, expect } from 'vitest'
import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import LangSwitcher from '@/components/header/LangSwitcher.vue'
import i18n from '@/tests/unit/config'

const localStorageMock = (function () {
  let store = {}
  return {
    getItem(key) {
      return store[key] || null
    },
    setItem(key, value) {
      store[key] = value.toString()
    },
    clear() {
      store = {}
    },
    removeItem(key) {
      delete store[key]
    }
  }
})()
Object.defineProperty(window, 'localStorage', {
  value: localStorageMock
})
Object.defineProperty(window, 'location', {
  value: {
    ...window.location,
    reload: vi.fn()
  },
  writable: true
})
describe('Template', () => {
  it('should have default element with one EN locale', async () => {
    const wrapper = shallowMount(LangSwitcher, {
      global: {
        plugins: [i18n]
      }
    })
    await nextTick()
    expect(wrapper.html()).contains('class="name">FR</span>')
    expect(wrapper.html()).contains('>EN</span>')
  })
  describe('Event', () => {
    describe('At click on the local button', () => {
      it('should call changeLocale on button click', async () => {
        const wrapper = shallowMount(LangSwitcher, {
          global: {
            plugins: [i18n]
          }
        })

        await nextTick()

        const dropdownBlock = wrapper.find('.dropdown-block')
        const buttons = dropdownBlock.findAll('button')

        const changeLocaleSpy = vi.spyOn(wrapper.vm, 'changeLocale')

        await buttons[0].trigger('click')

        expect(changeLocaleSpy).toHaveBeenCalled()
        expect(changeLocaleSpy).toHaveBeenCalledWith('en')
        expect(localStorage.getItem('user-locale')).toBe('en')
      })
    })
  })
})

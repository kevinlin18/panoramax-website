import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import AccountButton from '@/components/header/AccountButton.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(AccountButton, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.menuIsClosed).toBe(true)
      expect(wrapper.vm.isLogged).toBe(false)
      expect(wrapper.vm.userName).toBe(null)
      expect(wrapper.vm.ariaLabel).toBe(null)
    })
    it('should have all props filled', () => {
      const wrapper = shallowMount(AccountButton, {
        global: {
          plugins: [i18n]
        },
        props: {
          menuIsClosed: false,
          isLogged: true,
          userName: 'User',
          ariaLabel: 'label'
        }
      })
      expect(wrapper.html()).contains('aria-label="label"')
      expect(wrapper.html()).contains('class="wrapper-name"')
      expect(wrapper.html()).contains('User')
    })
  })
  describe('Events', () => {
    it('should emit triggerToggleMenu on button click', async () => {
      const wrapper = shallowMount(AccountButton, {
        global: {
          plugins: [i18n]
        }
      })

      await wrapper.find('button').trigger('click')
      expect(wrapper.emitted()).toHaveProperty('triggerToggleMenu')
    })
  })
})

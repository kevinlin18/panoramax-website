import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import LoginRegisterButton from '@/components/header/LoginRegisterButton.vue'
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => vi.fn()
}))
describe('Template', () => {
  it('should render the component without notifications', async () => {
    import.meta.env.VITE_API_URL = 'api-url/'
    const wrapper = shallowMount(LoginRegisterButton, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains(
      'pathexternal="api-url/api/auth/login?next_url='
    )
  })
})

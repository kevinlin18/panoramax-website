import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Notifications from '@/components/header/Notifications.vue'
vi.mock('@/store/upload', () => ({
  useUploadStore: vi.fn(() => ({
    uploadSets: []
  }))
}))

describe('Template', () => {
  it('should render the component without notifications', async () => {
    const wrapper = shallowMount(Notifications, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.text()).toContain('')
  })
})

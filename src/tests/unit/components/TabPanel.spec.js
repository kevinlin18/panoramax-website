import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import TabPanel from '@/components/TabPanel.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(TabPanel, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.panelSelected).toBe('')
      expect(wrapper.vm.isLoading).toBe(false)
      expect(wrapper.vm.isSequenceOwner).toBe(false)
    })
    describe('When props are filled', () => {
      describe('When there is less than 2 pictures', () => {
        it('should not render the the sort panel tab', () => {
          const wrapper = shallowMount(TabPanel, {
            global: {
              plugins: [i18n]
            },
            props: {
              picturesCount: 1,
              isSequenceOwner: true
            }
          })
          expect(wrapper.html()).contains(
            'class="tablinks">Gérer les photos</button>'
          )
          expect(wrapper.html()).contains(
            'class="tablinks">Régler l\'orientation</button>'
          )
          expect(wrapper.html()).not.contains(
            'class="tablinks">Trier la séquence</button>'
          )
        })
      })
      describe('When the user is not the sequence owner', () => {
        it('should render the component only with the manage picture tab', () => {
          const wrapper = shallowMount(TabPanel, {
            global: {
              plugins: [i18n]
            },
            props: {
              isSequenceOwner: false
            }
          })
          expect(wrapper.html()).contains(
            'class="tablinks">Gérer les photos</button>'
          )
          expect(wrapper.html()).not.contains(
            'class="tablinks">Régler l\'orientation</button>'
          )
          expect(wrapper.html()).not.contains(
            'class="tablinks">Trier la séquence</button>'
          )
        })
      })
      describe('When the user is the sequence owner', () => {
        it('should render the component with all the tabs', () => {
          const wrapper = shallowMount(TabPanel, {
            global: {
              plugins: [i18n]
            },
            props: {
              picturesCount: 2,
              isSequenceOwner: true
            }
          })
          expect(wrapper.html()).contains(
            'class="tablinks">Gérer les photos</button>'
          )
          expect(wrapper.html()).contains(
            'class="tablinks">Régler l\'orientation</button>'
          )
          expect(wrapper.html()).contains(
            'class="tablinks">Trier la séquence</button>'
          )
        })
        describe('When the orientation panel is selected', () => {
          it('should render the component in the orientation mode', () => {
            const wrapper = shallowMount(TabPanel, {
              global: {
                plugins: [i18n]
              },
              props: {
                isSequenceOwner: true,
                panelSelected: 'orientation'
              }
            })
            expect(wrapper.html()).contains(
              'class="tablinks selected">Régler l\'orientation</button>'
            )
          })
        })
        describe('When the component is loading', () => {
          it('should render all the tabs disabled', () => {
            const wrapper = shallowMount(TabPanel, {
              global: {
                plugins: [i18n]
              },
              props: {
                picturesCount: 2,
                isSequenceOwner: true,
                isLoading: true
              }
            })
            expect(wrapper.html()).contains(
              'disabled="">Gérer les photos</button>'
            )
            expect(wrapper.html()).contains(
              'disabled="">Régler l\'orientation</button>'
            )
            expect(wrapper.html()).contains(
              'disabled="">Trier la séquence</button>'
            )
          })
        })
      })
    })
  })
  describe('Event', () => {
    describe('When the user is the sequence owner', () => {
      it('should click on the orientation tab', async () => {
        const wrapper = shallowMount(TabPanel, {
          global: {
            plugins: [i18n]
          },
          props: {
            isSequenceOwner: true
          }
        })
        const buttonSort = wrapper.find('[data-test="button-orientation"]')
        expect(buttonSort.exists()).toBe(true)
        await buttonSort.trigger('click')
        expect(wrapper.emitted()).toHaveProperty('trigger')
        expect(wrapper.emitted().trigger[0]).toEqual(['orientation'])
      })
      it('should click on the sort tab', async () => {
        const wrapper = shallowMount(TabPanel, {
          global: {
            plugins: [i18n]
          },
          props: {
            isSequenceOwner: true,
            picturesCount: 2
          }
        })
        const buttonSort = wrapper.find('[data-test="button-sort"]')
        expect(buttonSort.exists()).toBe(true)
        await buttonSort.trigger('click')
        expect(wrapper.emitted()).toHaveProperty('trigger')
        expect(wrapper.emitted().trigger[0]).toEqual(['sort'])
      })
      it('should click on the photos tab', async () => {
        const wrapper = shallowMount(TabPanel, {
          global: {
            plugins: [i18n]
          },
          props: {
            isSequenceOwner: true
          }
        })
        const buttonSort = wrapper.find('[data-test="button-photos"]')
        expect(buttonSort.exists()).toBe(true)
        await buttonSort.trigger('click')
        expect(wrapper.emitted()).toHaveProperty('trigger')
        expect(wrapper.emitted().trigger[0]).toEqual(['photos'])
      })
    })
  })
})

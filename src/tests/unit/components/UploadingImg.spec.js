import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadingImg from '@/components/UploadingImg.vue'

describe('Template', () => {
  it('should have default images', () => {
    const wrapper = shallowMount(UploadingImg, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('/assets/images/uploading-person.svg')
    expect(wrapper.html()).contains('/assets/images/uploading-load.svg')
  })
})

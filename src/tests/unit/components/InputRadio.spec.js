import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InputRadio from '@/components/InputRadio.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(InputRadio, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('type="radio"')
      expect(wrapper.vm.name).toBe(null)
      expect(wrapper.vm.label).toBe('')
      expect(wrapper.vm.value).toBe(null)
      expect(wrapper.vm.id).toBe('')
    })
    describe('When the props are filled', () => {
      it('should render the component with a label, a name, a value and an Id', () => {
        const wrapper = shallowMount(InputRadio, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            label: 'my label',
            id: 'my id',
            name: 'my name'
          }
        })
        expect(wrapper.html()).contains('class="label"')
        expect(wrapper.html()).contains('my label')
        expect(wrapper.html()).contains('id="my id"')
        expect(wrapper.html()).contains('name="my name"')
        expect(wrapper.html()).contains('value="my id"')
      })
    })
  })
  describe('When the input is trigger', () => {
    it('should emit', () => {
      const wrapper = shallowMount(InputRadio, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          label: 'my label',
          id: 'my id',
          name: 'my name'
        }
      })
      wrapper.vm.$emit('trigger', 'my id')
      expect(wrapper.emitted().trigger).toBeTruthy()
      expect(wrapper.emitted().trigger[0][0]).toEqual('my id')
    })
  })
})

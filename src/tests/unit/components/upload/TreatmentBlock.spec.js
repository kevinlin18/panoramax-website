import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import TreatmentBlock from '@/components/upload/TreatmentBlock.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props and default wordings', () => {
      const wrapper = shallowMount(TreatmentBlock, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.progressClasse).toBe('')
      expect(wrapper.vm.errorPercentage).toBe(0)
      expect(wrapper.vm.percentage).toBe(0)
      expect(wrapper.vm.uploadSets).toStrictEqual(null)
      expect(wrapper.html()).contains('pages.upload.no-process_desc')
      expect(wrapper.html()).contains(
        'percentage="0" errorpercentage="0" itemsbroken="0" itemsrejected="0"'
      )
    })
    describe('When the props percentage is 100', () => {
      it('should render the component completed', () => {
        const wrapper = shallowMount(TreatmentBlock, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            percentage: 100
          }
        })
        expect(wrapper.html()).contains('pages.upload.processed_desc')
        expect(wrapper.html()).contains(
          'percentage="100" errorpercentage="0" itemsbroken="0" itemsrejected="0"'
        )
      })
    })
    describe('When the props percentage is 10', () => {
      it('should render the component processing', () => {
        const wrapper = shallowMount(TreatmentBlock, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            percentage: 10
          }
        })
        expect(wrapper.html()).contains('pages.upload.processing_desc')
        expect(wrapper.html()).contains(
          'percentage="10" errorpercentage="0" itemsbroken="0" itemsrejected="0"'
        )
      })
    })
    describe('When the props progressClasse is filled', () => {
      it('should render the component with the class', () => {
        const wrapper = shallowMount(TreatmentBlock, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            progressClasse: 'progress-bar-processing'
          }
        })
        expect(wrapper.html()).contains(
          '<p class="progress-bar-processing">pages.upload.no-process_desc</p>'
        )
      })
    })
  })
})

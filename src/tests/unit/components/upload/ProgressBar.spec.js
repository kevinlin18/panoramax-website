import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import ProgressBar from '@/components/upload/ProgressBar.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(ProgressBar, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.percentage).toBe(0)
      expect(wrapper.html()).contains('Aucun floutage en cours')
      expect(wrapper.html()).contains('class="progress-bar no-progress-bar"')
    })
    describe('When the prop percentage are filled', () => {
      it('should render the component with the bar progressing', () => {
        const wrapper = shallowMount(ProgressBar, {
          global: {
            plugins: [i18n]
          },
          props: {
            percentage: 10
          }
        })
        expect(wrapper.html()).contains('style="width: 10%;"')
        expect(wrapper.html()).contains('>10% </span></div>')
      })
      it('should render the component with the bar completed', () => {
        const wrapper = shallowMount(ProgressBar, {
          global: {
            plugins: [i18n]
          },
          props: {
            percentage: 100
          }
        })
        expect(wrapper.html()).contains('style="width: 100%;"')
        expect(wrapper.html()).contains('Floutage terminé')
        expect(wrapper.html()).contains(
          'class="progress-bar progress-bar-completed"'
        )
      })
    })
  })
})

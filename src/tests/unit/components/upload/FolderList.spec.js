import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import FolderList from '@/components/upload/FolderList.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(FolderList, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.pictures).toEqual([])
      expect(wrapper.vm.currentFolder).toBe(null)
    })
    describe('When the prop percentage are filled', () => {
      it('should render the component with a folder list', () => {
        const wrapper = shallowMount(FolderList, {
          global: {
            plugins: [i18n]
          },
          props: {
            pictures: [{ folderName: 'folder', files: [{}, {}] }],
            currentFolder: 'currentFolder'
          }
        })
        expect(wrapper.vm.pictures).toEqual([
          { folderName: 'folder', files: [{}, {}] }
        ])
        expect(wrapper.vm.currentFolder).toBe('currentFolder')
        expect(wrapper.html()).contains('📁 folder')
        expect(wrapper.html()).contains('2 Photos')
      })
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadErrorMessage from '@/components/upload/UploadErrorMessage.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(UploadErrorMessage, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.uploadError).toBe(null)
    })
    describe('When props are filled', () => {
      it('should render the component with the upload error', () => {
        const wrapper = shallowMount(UploadErrorMessage, {
          global: {
            plugins: [i18n]
          },
          props: {
            uploadError: 'error'
          }
        })
        expect(wrapper.html()).contains('>error</p>')
      })
    })
  })
})

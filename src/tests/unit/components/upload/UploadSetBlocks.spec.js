import { it, describe, expect, vi } from 'vitest'
import { shallowMount, flushPromises } from '@vue/test-utils'
import * as deleteAnUploadSet from '@/views/utils/sequence/request'
import UploadSetBlocks from '@/components/upload/UploadSetBlocks.vue'
import i18n from '@/tests/unit/config'
import { createPinia } from 'pinia'
const pinia = createPinia()
vi.mock('vue-router', () => ({
  useRoute: () => ({
    path: '/mock-path'
  }),
  useRouter: () => ({
    push: vi.fn()
  })
}))
vi.mock('@/views/utils/upload/request', () => ({
  fetchUploadSets: vi.fn(() =>
    Promise.resolve({
      data: {
        upload_sets: [
          {
            id: 1,
            title: 'Upload Set 1',
            nb_items: 2,
            created_at: '2024-10-10T10:00:00Z'
          },
          {
            id: 2,
            title: 'Upload Set 2',
            nb_items: 3,
            created_at: '2024-10-11T12:00:00Z'
          }
        ]
      }
    })
  )
}))
describe('UploadSetBlocks', () => {
  describe('Template', () => {
    describe('When the datas are fetched', () => {
      it('should display the unfinished uploadSets upload', async () => {
        const wrapper = shallowMount(UploadSetBlocks, {
          global: {
            plugins: [i18n, pinia]
          }
        })
        await flushPromises()
        expect(wrapper.html()).contains('Mes envois non terminés')
        expect(wrapper.html()).contains('class="bi bi-chevron-up"')
        expect(wrapper.html()).contains('Upload Set 1')
        expect(wrapper.html()).contains('<notifications')
        expect(wrapper.html()).contains('10 Oct 2024 10:00:00')
        expect(wrapper.html()).contains('Upload Set 2')
        expect(wrapper.html()).contains('11 Oct 2024 12:00:00')
        expect(wrapper.html()).contains('icon="bi bi-trash"')
        expect(wrapper.html()).contains('text="Supprimer"')
        expect(wrapper.html()).contains('icon="bi bi-eye"')
        expect(wrapper.html()).contains('text="Voir"')
      })
      describe('When the upload set button is clicked', () => {
        it('should display the list of upload set', async () => {
          const wrapper = shallowMount(UploadSetBlocks, {
            global: {
              plugins: [i18n, pinia]
            }
          })
          await flushPromises()
          expect(wrapper.html()).contains('class="bi bi-chevron-up"')
          expect(wrapper.vm.blockIsOpen).toBe(false)
          await wrapper.find('button').trigger('click')
          expect(wrapper.vm.blockIsOpen).toBe(true)
          expect(wrapper.html()).contains('class="bi bi-chevron-down"')
        })
      })
    })
    describe('Event', () => {
      describe('When the redirect button is clicked', () => {
        it('should call the seeAndRedirect function and redirect', async () => {
          const wrapper = shallowMount(UploadSetBlocks, {
            global: {
              plugins: [i18n, pinia]
            }
          })
          await flushPromises()
          const spy = vi.spyOn(wrapper.vm, 'seeAndRedirect')
          const linkSee = wrapper.findComponent('[data-test="link-see-2"]')
          await linkSee.vm.$emit('trigger')
          expect(spy).toHaveBeenCalled()
        })
      })
      describe('When the delete button is clicked and the delete confirmed', () => {
        it('should remove the uploadSet', async () => {
          const confirmMock = vi.spyOn(window, 'confirm').mockReturnValue(true)
          const wrapper = shallowMount(UploadSetBlocks, {
            global: {
              plugins: [i18n, pinia]
            }
          })
          await flushPromises()
          const spy = vi.spyOn(wrapper.vm, 'deleteUploadSet')
          const buttonDelete = wrapper.findComponent(
            '[data-test="button-delete-2"]'
          )
          const spyDeleteAnUploadSet = vi.spyOn(
            deleteAnUploadSet,
            'deleteAnUploadSet'
          )
          spyDeleteAnUploadSet.mockResolvedValue({})
          await buttonDelete.vm.$emit('trigger')
          await flushPromises()
          expect(spy).toHaveBeenCalled()
          expect(confirmMock).toHaveBeenCalledWith(
            '⚠️ Le téléchargement et toutes les séquences qui y sont associées vont être définitivement supprimés'
          )
          expect(wrapper.html()).contains('data-test="button-delete-1"')
          expect(wrapper.html()).contains('data-test="link-see-1"')
          expect(wrapper.html()).not.contains('data-test="button-delete-2"')
          expect(wrapper.html()).not.contains('data-test="link-see-2"')
        })
      })
      describe('When the delete button is clicked and the delete not confirmed', () => {
        it('should not remove the uploadSet', async () => {
          const confirmMock = vi.spyOn(window, 'confirm').mockReturnValue(false)
          const wrapper = shallowMount(UploadSetBlocks, {
            global: {
              plugins: [i18n, pinia]
            }
          })
          await flushPromises()
          const spy = vi.spyOn(wrapper.vm, 'deleteUploadSet')
          const buttonDelete = wrapper.findComponent(
            '[data-test="button-delete-2"]'
          )
          const spyDeleteAnUploadSet = vi.spyOn(
            deleteAnUploadSet,
            'deleteAnUploadSet'
          )
          spyDeleteAnUploadSet.mockResolvedValue({})
          await buttonDelete.vm.$emit('trigger')
          await flushPromises()
          expect(spy).toHaveBeenCalled()
          expect(confirmMock).toHaveBeenCalledWith(
            '⚠️ Le téléchargement et toutes les séquences qui y sont associées vont être définitivement supprimés'
          )
          expect(wrapper.html()).contains('data-test="button-delete-1"')
          expect(wrapper.html()).contains('data-test="link-see-1"')
          expect(wrapper.html()).contains('data-test="button-delete-2"')
          expect(wrapper.html()).contains('data-test="link-see-2"')
        })
      })
    })
  })
})

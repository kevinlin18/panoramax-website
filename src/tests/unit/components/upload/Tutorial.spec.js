import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Tutorial from '@/components/upload/Tutorial.vue'

describe('Template', () => {
  it('should render the component', async () => {
    const wrapper = shallowMount(Tutorial, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('sequence_tutorial_text')
    expect(wrapper.html()).contains('assets/images/android-upload-tutorial.jpg')
    expect(wrapper.html()).contains(
      'assets/images/android-upload-tutorial-files.jpg'
    )
  })
})

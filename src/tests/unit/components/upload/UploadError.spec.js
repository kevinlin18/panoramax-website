import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadError from '@/components/upload/UploadError.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(UploadError, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.count).toBe(0)
      expect(wrapper.vm.pictures).toStrictEqual([])
    })
    describe('When the props are filled', () => {
      it('should render the component with the texts', async () => {
        const wrapper = shallowMount(UploadError, {
          global: {
            plugins: [i18n]
          },
          props: {
            count: 10,
            pictures: [
              {
                message: 'Métdata invalides',
                name: '25bc-4e4a-a360-8c4e54146b9d.jpg',
                reason: 'invalid_metadata'
              }
            ]
          }
        })
        wrapper.vm.isOpen = true
        await wrapper.vm.$nextTick()
        expect(wrapper.html()).contains('upload-error-block-stub')
        expect(wrapper.html()).contains('isopen="true"')
      })
    })
  })
  describe('When the chevron button is clicked', () => {
    it('should open the error block', async () => {
      const wrapper = shallowMount(UploadError, {
        global: {
          plugins: [i18n]
        },
        props: {
          count: 10,
          reason: 'invalid_metadata',
          message: 'message',
          pictures: [
            {
              message: 'Métdata invalides',
              name: '25bc-4e4a-a360-8c4e54146b9d.jpg',
              reason: 'invalid_metadata'
            }
          ]
        }
      })
      expect(wrapper.html()).contains('class="bi bi-chevron-up"')
      expect(wrapper.vm.isOpen).toBe(false)
      await wrapper.find('button').trigger('click')
      expect(wrapper.vm.isOpen).toBe(true)
      expect(wrapper.html()).contains('class="bi bi-chevron-down"')
    })
  })
})

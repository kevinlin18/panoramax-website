import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import SequenceSection from '@/components/upload/SequenceSection.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props and default wordings', () => {
      const wrapper = shallowMount(SequenceSection, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.listDisplayed).toBe(false)
      expect(wrapper.vm.isLoading).toBe(false)
      expect(wrapper.vm.isLoaded).toBe(false)
      expect(wrapper.vm.newSeqTitle).toBe('')
      expect(wrapper.vm.uploadSets).toStrictEqual(null)
      expect(wrapper.html()).contains('pages.upload.my_sequences_title')
      expect(wrapper.html()).contains('pages.upload.my_sequences_subtitle')
      expect(wrapper.html()).contains('pages.upload.my_sequences_status')
      expect(wrapper.html()).contains('pages.upload.my_sequences_pictures')
      expect(wrapper.html()).contains('pages.upload.my_sequences_pictures_date')
      expect(wrapper.html()).contains('pages.upload.my_sequences_description')
      expect(wrapper.html()).contains('pages.upload.creating_sequences_process')
    })
    describe('When the component have the listDisplayed and uploadSets props filled', () => {
      it('should rendez the component with the list', () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        const uploadSets = {
          id: 'upload-set-id',
          created_at: '2024-08-20T07:37:15.504089Z',
          completed: true,
          dispatched: true,
          account_id: 'account-id',
          title: 'Envoi du 20 août 2024, 09:37:15',
          estimated_nb_files: 4,
          sort_method: 'time-asc',
          split_distance: 100,
          split_time: 60,
          duplicate_distance: 1,
          duplicate_rotation: 30,
          associated_collections: [
            {
              id: 'collection-id',
              nb_items: 4,
              extent: {
                temporal: {
                  interval: [['2021-12-14T15:01:34Z', '2021-12-14T15:01:40Z']]
                }
              },
              title: 'Envoi du 20 août 2024, 09:37:15',
              items_status: {
                prepared: 4,
                rejected: 0,
                broken: 0,
                not_processed: 0
              },
              links: [
                {
                  rel: 'self',
                  type: 'application/json',
                  href: 'http://localhost:5000/api/collections/0906c052-8e10-43f7-8de6-229d552ab914'
                }
              ],
              ready: true
            }
          ],
          nb_items: 4,
          items_status: {
            prepared: 4,
            preparing: 0,
            broken: 0,
            rejected: 0,
            not_processed: 0
          },
          links: [
            {
              rel: 'self',
              type: 'application/json',
              href: 'http://localhost:5000/api/upload_sets/6b8bc311-b06e-40ee-9bb9-2d41fc67589a'
            }
          ],
          ready: true
        }

        const wrapper = shallowMount(SequenceSection, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            listDisplayed: true,
            newSeqTitle: 'title',
            uploadSets
          }
        })
        expect(wrapper.vm.listDisplayed).toBe(true)
        expect(wrapper.vm.uploadSets).toStrictEqual(uploadSets)
        expect(wrapper.html()).contains('class="collection-status-item"')
        expect(wrapper.html()).contains('count="4"')
        expect(wrapper.html()).contains(
          'href="api-url/api/collections/collection-id/thumb.jpg"'
        )
        expect(wrapper.html()).contains('defaulttext="title"')
        expect(wrapper.html()).contains('status="ready"')
        expect(wrapper.html()).contains(
          'firstdate="Tue Dec 14 2021 15:01:34 GMT+0000 (Greenwich Mean Time)"'
        )
        expect(wrapper.html()).contains(
          'lastdate="Tue Dec 14 2021 15:01:40 GMT+0000 (Greenwich Mean Time)"'
        )
        expect(wrapper.html()).contains('text="pages.upload.go_to_sequence"')
        expect(wrapper.html()).contains('status="ready"')
      })
    })
    describe('When the component have the isLoading and isLoaded props are filled', () => {
      it('should render the component with props values', () => {
        const wrapper = shallowMount(SequenceSection, {
          global: {
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isLoading: true,
            isLoaded: true
          }
        })
        expect(wrapper.vm.isLoading).toBe(true)
        expect(wrapper.vm.isLoaded).toBe(true)
      })
    })
  })
})

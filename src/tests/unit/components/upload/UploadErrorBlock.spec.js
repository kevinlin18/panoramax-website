import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadErrorBlock from '@/components/upload/UploadErrorBlock.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(UploadErrorBlock, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.isOpen).toBe(false)
      expect(wrapper.vm.errors).toStrictEqual([])
    })
    describe('When the props are filled', () => {
      it('should render the component with the texts', () => {
        const wrapper = shallowMount(UploadErrorBlock, {
          global: {
            plugins: [i18n]
          },
          props: {
            errors: [
              {
                message: 'Métdata invalides',
                severity: 'error',
                name: '25bc-4e4a-a360-8c4e54146b9d.jpg',
                reason: 'capture_duplicate'
              }
            ]
          }
        })
        expect(wrapper.html()).contains(
          'Cette photo est très similaire avec une autre (1)'
        )
        expect(wrapper.html()).contains(
          "Peut-être avez-vous passé du temps au feu rouge ? Toute photo trop proche d'une autre est ignorée."
        )
        expect(wrapper.html()).contains('class="bi bi-chevron-up"')
      })
    })
  })
  describe('Event', () => {
    describe('When the open button is clicked', () => {
      it('should open the panel with all picture with errors', async () => {
        const wrapper = shallowMount(UploadErrorBlock, {
          global: {
            plugins: [i18n]
          },
          props: {
            errors: [
              {
                message: 'Métdata invalides',
                severity: 'error',
                name: '25bc-4e4a-a360-8c4e54146b9d.jpg',
                reason: 'file_duplicate'
              }
            ]
          }
        })
        const buttonWrapper = wrapper.find('[data-test="button-open-error"]')
        await buttonWrapper.trigger('click')
        expect(wrapper.html()).contains(
          'Vous avez déjà envoyé la même photo par le passé (1) '
        )
        expect(wrapper.html()).contains(
          'Toute photo détectée comme doublon est ignorée.'
        )
        expect(wrapper.html()).contains('class="bi bi-chevron-down"')
        expect(wrapper.html()).contains('25bc-4e4a-a360-8c4e54146b9d.jpg')
      })
    })
  })
})

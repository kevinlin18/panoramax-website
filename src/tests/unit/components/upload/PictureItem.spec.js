import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import PictureItem from '@/components/upload/PictureItem.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(PictureItem, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.name).toBe('')
      expect(wrapper.vm.text).toStrictEqual('')
      expect(wrapper.vm.look).toBe('default')
    })
  })
  describe('When the component have a look', () => {
    it('should have a specific class', () => {
      const wrapper = shallowMount(PictureItem, {
        global: {
          plugins: [i18n]
        },
        props: {
          look: 'success'
        }
      })
      expect(wrapper.html()).contains('class="uploaded-picture-item success"')
    })
  })
  describe('When the component have a name and a text', () => {
    it('should have a specific class', () => {
      const wrapper = shallowMount(PictureItem, {
        global: {
          plugins: [i18n]
        },
        props: {
          name: 'name',
          text: 'text'
        }
      })
      expect(wrapper.html()).contains('class="uploaded-information"')
      expect(wrapper.html()).contains('name - text')
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import SequenceStatus from '@/components/SequenceStatus.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(SequenceStatus, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.status).toBe(null)
      expect(wrapper.vm.isDefaultOpen).toBe(true)
    })
    describe('When props are filled', () => {
      it('should render the component with the status ready', () => {
        const wrapper = shallowMount(SequenceStatus, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            status: 'ready'
          }
        })
        expect(wrapper.html()).contains('Publiée')
        expect(wrapper.html()).contains('class="sequence-status ready open"')
      })
      it('should render the component closed', () => {
        const wrapper = shallowMount(SequenceStatus, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          },
          props: {
            isDefaultOpen: false
          }
        })
        expect(wrapper.html()).contains('class="sequence-status"')
        expect(wrapper.html()).contains('En cours de floutage')
      })
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Loader from '@/components/Loader.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Loader, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.look).toBe('sm')
      expect(wrapper.vm.color).toBe('blue')
      expect(wrapper.vm.isLoaded).toBe(true)
    })
  })
  describe('When the component is loading', () => {
    it('should not have the loaded class', () => {
      const wrapper = shallowMount(Loader, {
        global: {
          plugins: [i18n]
        },
        props: {
          isLoaded: false
        }
      })
      expect(wrapper.html()).contains('class="lds-ring sm blue"')
    })
  })
  describe('When the component have a look', () => {
    it('should have a look classe', () => {
      const wrapper = shallowMount(Loader, {
        global: {
          plugins: [i18n]
        },
        props: {
          look: 'md'
        }
      })
      expect(wrapper.html()).contains('class="lds-ring md blue loaded"')
    })
  })
})

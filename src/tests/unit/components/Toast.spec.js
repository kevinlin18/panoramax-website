import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Toast from '@/components/Toast.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Toast, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.text).toBe('')
      expect(wrapper.vm.look).toStrictEqual('')
    })
  })
  describe('When the component have look and text filled', () => {
    it('should render the component with the error and the display class', () => {
      const wrapper = shallowMount(Toast, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          text: 'my text error',
          look: 'error'
        }
      })
      expect(wrapper.html()).contains('class="toast-wrapper error display"')
      expect(wrapper.html()).contains('class="bi bi-exclamation-triangle"')
      expect(wrapper.html()).contains('my text error')
    })

    it('should render the component with the success and the display class', () => {
      const wrapper = shallowMount(Toast, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          text: 'my text success',
          look: 'success'
        }
      })
      expect(wrapper.html()).contains('class="toast-wrapper success display"')
      expect(wrapper.html()).contains('class="bi bi-check-circle"')
      expect(wrapper.html()).contains('my text success')
    })
  })
  describe('When the button is trigger', () => {
    it('should emit', () => {
      const wrapper = shallowMount(Toast, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      wrapper.vm.$emit('trigger')

      expect(wrapper.emitted().trigger).toBeTruthy()
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Pagination from '@/components/Pagination.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(Pagination, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.vm.href).toBe(null)
      expect(wrapper.vm.selfLink).toStrictEqual({})
      expect(wrapper.vm.type).toBe('')
    })
  })
  describe('When the component is the last or the first page', () => {
    it('should render the component without border', () => {
      const wrapper = shallowMount(Pagination, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          type: 'double-left'
        }
      })
      expect(wrapper.html()).contains('class="wrapper-pagination no-border"')
    })
  })
  describe('When the component is disabled', () => {
    it('should render the component a disabled state', () => {
      const href = 'my-url'
      const wrapper = shallowMount(Pagination, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          type: 'double-left',
          href,
          selfLink: { href }
        }
      })
      expect(wrapper.html()).contains('class="pagination-button disabled"')
    })
  })
  describe('When the button is trigger', () => {
    it('should emit', () => {
      const href = 'my-url'
      const wrapper = shallowMount(Pagination, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          type: 'left',
          href,
          selfLink: { href: 'self-link' }
        }
      })
      wrapper.vm.$nextTick()
      wrapper.vm.$emit('trigger', href)

      expect(wrapper.emitted().trigger).toBeTruthy()
      expect(wrapper.emitted().trigger[0][0]).toEqual(href)
    })
  })
})

import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InputUpload from '@/components/InputUpload.vue'
import i18n from '@/tests/unit/config'
describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.isLogged).toBe(false)
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.textPictureType).toBe(null)
      expect(wrapper.vm.textSecondPart).toBe(null)
    })
    it('should have all the props filled', () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        },
        props: {
          isLogged: true,
          text: 'my text',
          textPictureType: 'my textPictureType',
          textSecondPart: 'my textSecondPart'
        }
      })
      expect(wrapper.html()).contains('my text')
      expect(wrapper.html()).contains('my textSecondPart')
      expect(wrapper.html()).contains('my textPictureType')
    })
  })
  describe('When the input is dragover', () => {
    it('should set the component dragging state', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        },
        props: {
          isLogged: true
        }
      })
      await wrapper.find('label').trigger('dragover')
      expect(wrapper.vm.isDragging).toBe(true)
      expect(wrapper.html()).contains('class="file-upload dragging"')
    })
  })
  describe('When the input is dragleave', () => {
    it('should set the component dragging state', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        },
        props: {
          isLogged: true
        }
      })
      await wrapper.find('label').trigger('dragover')
      await wrapper.find('label').trigger('dragleave')
      expect(wrapper.vm.isDragging).toBe(false)
      expect(wrapper.html()).contains('class="file-upload"')
    })
  })
  describe('When the input is dropped', () => {
    it('should emit files with value', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      const mockFiles = [
        new File(['dummy content'], 'image1.jpeg', { type: 'image/jpeg' }),
        new File(['dummy content'], 'image2.jpeg', { type: 'image/jpeg' })
      ]

      const mockDataTransfer = {
        files: mockFiles,
        items: mockFiles.map((file) => ({
          kind: 'file',
          webkitGetAsEntry: () => ({
            isFile: true,
            isDirectory: false,
            file: (callback) => callback(file)
          })
        }))
      }

      await wrapper.trigger('drop', {
        dataTransfer: mockDataTransfer
      })

      expect(wrapper.emitted().trigger).toBeTruthy()
      expect(wrapper.emitted().trigger[0][0]).toEqual(mockFiles)
    })
    it('should emit directory with files with value', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      const mockFile = new File(['dummy content'], 'image3.jpeg', {
        type: 'image/jpeg'
      })

      const mockFileEntry = {
        isFile: true,
        isDirectory: false,
        file: (cb) => cb(mockFile)
      }

      const mockDirectoryEntry = {
        isFile: false,
        isDirectory: true,
        name: 'mockDirectory',
        createReader: () => ({
          readEntries: (cb) => {
            cb([mockFileEntry])
            cb([])
          }
        })
      }

      const mockDataTransfer = {
        items: [
          {
            kind: 'file',
            webkitGetAsEntry: () => mockDirectoryEntry
          }
        ]
      }

      await wrapper.trigger('drop', {
        dataTransfer: mockDataTransfer
      })

      const emittedEvent = wrapper.emitted().trigger
      expect(emittedEvent).toBeTruthy()

      const emittedValue = emittedEvent[0][0]
      expect(emittedValue).toEqual([
        {
          folderName: 'mockDirectory',
          files: []
        }
      ])
    })
  })
})

import { it, describe, beforeEach, expect, vi } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'
import { useSequenceStore } from '@/store/sequence' // adjust the path as necessary

describe('Sequence Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('should have default state', () => {
    const store = useSequenceStore()
    expect(store.toastText).toBe('')
    expect(store.toastLook).toBe('')
    expect(store.picId).toBe('')
  })

  it('addToastText should update toastText and toastLook, then reset toastText after timeout', async () => {
    vi.useFakeTimers()
    const store = useSequenceStore()

    store.addToastText('New Toast', 'success')

    expect(store.toastText).toBe('New Toast')
    expect(store.toastLook).toBe('success')

    vi.advanceTimersByTime(5500)

    expect(store.toastText).toBe('')
    vi.useRealTimers()
  })

  it('addSequence should update picId', () => {
    const store = useSequenceStore()

    store.addSequence('1234')

    expect(store.picId).toBe('1234')
  })
})

import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import SharePicturesView from '@/views/SharePicturesView.vue'
import i18n from '@/tests/unit/config'
import { createRouter, createWebHistory } from 'vue-router'
import * as authConfigModule from '@/composables/auth'

const router = createRouter({
  history: createWebHistory(),
  routes: [{ path: '/', component: { template: '<div></div>' } }]
})
const authConfigMock = vi.spyOn(authConfigModule, 'authConfig')
authConfigMock.mockReturnValue({})
describe('Template', () => {
  it('should render the view with the button link', async () => {
    const wrapper = shallowMount(SharePicturesView, {
      global: {
        plugins: [i18n, router],
        mocks: {
          $t: (msg) => msg,
          authConf: {
            enabled: true
          }
        }
      }
    })
    expect(wrapper.html()).contains('<link')
    expect(wrapper.html()).contains('pathexternal=')
    expect(wrapper.html()).contains('look="button button--blue"')
    expect(wrapper.html()).contains('type="external"')
  })
  it('should render the view without the button link', async () => {
    import.meta.env.VITE_API_URL = 'api-url/'
    const wrapper = shallowMount(SharePicturesView, {
      global: {
        plugins: [i18n, router],
        mocks: {
          $t: (msg) => msg,
          authConf: {
            enabled: false
          }
        }
      }
    })
    expect(wrapper.html()).not.toContain('pages.share_pictures.sub_title')
  })
})

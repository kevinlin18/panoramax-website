import { it, describe, expect, vi } from 'vitest'
import { ref } from 'vue'
import { shallowMount, mount, flushPromises } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
import { createRouter, createWebHistory } from 'vue-router'
import UploadPicturesView from '@/views/UploadPicturesView.vue'
import i18n from '@/tests/unit/config'
import InputUpload from '@/components/InputUpload.vue'
import * as createAPictureToAnUploadSet from '@/views/utils/upload/request'
import * as createUploadSets from '@/views/utils/upload/request'
import * as fetchUploadSet from '@/views/utils/upload/request'
import * as fetchUploadSetFiles from '@/views/utils/upload/request'
import { formatDate } from '@/utils/dates'
import * as sortByName from '@/views/utils/upload/index'
import * as authConfigModule from '@/composables/auth'
vi.mock('axios')
vi.mock('@/store/upload', () => ({
  useUploadStore: vi.fn(() => ({
    fetchUploadSets: vi.fn(),
    uploadSets: [],
    upload: ref([])
  }))
}))
vi.mock('vue-router', async (original) => ({
  ...(await original()),
  onBeforeRouteLeave: vi.fn()
}))
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: { template: '<div></div>' }
    }
  ]
})
const store = createTestingPinia({
  createSpy: vi.fn
})

const authConfigMock = vi.spyOn(authConfigModule, 'authConfig')
authConfigMock.mockReturnValue({})
describe('Template', () => {
  it('should render the view with the input upload and the good wordings', async () => {
    const wrapper = shallowMount(UploadPicturesView, {
      global: {
        plugins: [i18n, router, store],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    wrapper.vm.isLogged = true
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).contains('pages.upload.description')
    expect(wrapper.html()).contains('<input-upload-stub')
    expect(wrapper.html()).contains('pages.upload.input_label')
  })
  describe('trigger addPictures', () => {
    describe('submit uploadPicture', () => {
      it('should trigger to uploadPictures and display no pictures treatments', async () => {
        const wrapper = mount(UploadPicturesView, {
          global: {
            plugins: [i18n, router],
            mocks: {
              $t: (msg) => msg
            },
            components: {
              InputUpload
            }
          }
        })
        wrapper.vm.isLogged = true
        await wrapper.vm.$nextTick()
        const sortByNameMock = vi.spyOn(sortByName, 'sortByName')
        sortByNameMock.mockReturnValue([
          { type: 'image/jpeg' },
          { type: 'image/jpeg' }
        ])
        const wrapperInputUpload = wrapper.findComponent(InputUpload)
        await wrapperInputUpload.trigger('trigger')
        await wrapperInputUpload.vm.$emit('trigger', [
          { type: 'image/jpeg' },
          { type: 'image/jpeg' }
        ])
        wrapper.vm.isLoading = true
        wrapper.vm.isLoaded = false
        await wrapper.vm.$nextTick()
        expect(wrapper.html()).contains('class="wrapper-uploading"')
        expect(wrapper.html()).contains('pages.upload.uploading_process')
        expect(wrapper.html()).contains('pages.upload.leave_message')
        expect(wrapper.html()).contains('class="uploading-img"')
        expect(wrapper.html()).contains('class="loader-percentage"')
        expect(wrapper.html()).contains('class="lds-ring md blue"')
        expect(wrapper.html()).contains('pages.upload.no-process_desc')
        expect(wrapper.html()).contains('class="progress-bar no-progress-bar"')
        expect(wrapper.html()).contains('pages.upload.import_treatment_title')
        expect(wrapper.html()).contains('pages.upload.no_process')
      })
    })
    describe('one sequence has been imported', () => {
      it('should render a sequence with a list of uploaded pictures', async () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        vi.useRealTimers()
        const wrapper = mount(UploadPicturesView, {
          global: {
            plugins: [i18n, router],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        wrapper.vm.isLogged = true
        await wrapper.vm.$nextTick()
        const picture = {
          lastModified: 1599133968750,
          name: '100MSDCF_DSC02790.JPG',
          size: 2345,
          type: 'image/jpeg'
        }
        const uploadSet = {
          id: 'upload-set-id',
          created_at: '2024-08-20T07:37:15.504089Z',
          completed: true,
          dispatched: true,
          account_id: 'account-id',
          title: 'Envoi du 20 août 2024, 09:37:15',
          estimated_nb_files: 4,
          sort_method: 'time-asc',
          split_distance: 100,
          split_time: 60,
          duplicate_distance: 1,
          duplicate_rotation: 30,
          associated_collections: [
            {
              id: 'collection-id',
              nb_items: 4,
              extent: {
                temporal: {
                  interval: [['2021-12-14T15:01:34Z', '2021-12-14T15:01:40Z']]
                }
              },
              title: 'Envoi du 20 août 2024, 09:37:15',
              items_status: {
                prepared: 4,
                rejected: 0,
                broken: 0,
                not_processed: 0
              },
              links: [
                {
                  rel: 'self',
                  type: 'application/json',
                  href: 'http://localhost:5000/api/collections/0906c052-8e10-43f7-8de6-229d552ab914'
                }
              ],
              ready: true
            }
          ],
          nb_items: 4,
          items_status: {
            prepared: 4,
            preparing: 0,
            broken: 0,
            rejected: 0,
            not_processed: 0
          },
          links: [
            {
              rel: 'self',
              type: 'application/json',
              href: 'http://localhost:5000/api/upload_sets/6b8bc311-b06e-40ee-9bb9-2d41fc67589a'
            }
          ],
          ready: true
        }
        const spyAnUploadSet = vi.spyOn(createUploadSets, 'createUploadSets')
        const spyPicture = vi.spyOn(
          createAPictureToAnUploadSet,
          'createAPictureToAnUploadSet'
        )
        const sortByNameMock = vi.spyOn(sortByName, 'sortByName')
        sortByNameMock.mockReturnValue([picture])
        const uploadSetId = 'my-id'
        spyAnUploadSet.mockReturnValue({
          data: { id: uploadSetId, title: 'uploadset title' }
        })
        spyPicture.mockReturnValue({ data: {} })
        const uploadSetTitle = `Envoi du ${formatDate(
          new Date(),
          'LL'
        )}, ${formatDate(new Date(), 'HH:mm:ss')}`
        const body = new FormData()
        body.append('position', '1')
        body.append('picture', picture)
        const wrapperInputUpload = wrapper.findComponent(InputUpload)
        await wrapperInputUpload.trigger('trigger')
        await wrapper.findComponent(InputUpload).vm.$emit('trigger', [picture])
        const spyFetchAnUploadSetFiles = vi.spyOn(
          fetchUploadSetFiles,
          'fetchUploadSetFiles'
        )
        spyFetchAnUploadSetFiles.mockResolvedValue({
          data: {
            files: [
              {
                content_md5: '44abeef2e63152da2ea0508b2a148b84',
                file_name: '100MSDCF_DSC02790.JPG',
                file_type: 'picture',
                inserted_at: '2024-10-30T14:07:52.238966Z',
                picture_id: '7fa1dbd9-492d-4870-97ed-fe8408eca1e6',
                size: 2345
              }
            ]
          }
        })
        wrapper.vm.isLoading = true
        await wrapper.vm.$nextTick()
        await new Promise((resolve) => setTimeout(resolve, 1000))
        const spyFetchAnUploadSet = vi.spyOn(fetchUploadSet, 'fetchUploadSet')
        spyFetchAnUploadSet.mockResolvedValue({ data: uploadSet })
        await new Promise((resolve) => setTimeout(resolve, 1000))
        await flushPromises()
        expect(spyPicture).toHaveBeenCalled()
        expect(spyPicture).toHaveBeenCalledWith(
          uploadSetId,
          expect.any(FormData)
        )
        expect(spyAnUploadSet).toHaveBeenCalledWith({
          estimated_nb_files: 1,
          title: uploadSetTitle
        })
        expect(wrapper.html()).contains('class="loaded-block success"')
        expect(wrapper.html()).contains('4/4</span>')
        expect(wrapper.html()).contains('pages.upload.processed_desc')
        expect(wrapper.html()).contains(
          'class="progress-bar progress-bar-completed"'
        )
        expect(wrapper.html()).contains('pages.upload.processed_text')
        expect(wrapper.html()).contains('pages.upload.my_sequences_title')
        expect(wrapper.html()).contains('class="sequence-status ready open"')
        expect(wrapper.html()).contains('Publiée')
        expect(wrapper.html()).contains('pages.upload.go_to_sequence')
      })
    })
  })
})

import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import TermsOfService from '@/views/TermsOfService.vue'
import i18n from '@/tests/unit/config'
vi.mock('axios', () => ({
  default: {
    get: vi.fn().mockResolvedValue({ data: { languages: ['fr'] } }),
    defaults: { headers: { common: { 'Content-Type': 'text/html' } } }
  }
}))
describe('Template', () => {
  it('should have the default content', () => {
    const wrapper = shallowMount(TermsOfService, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg,
          content: '<p>my-tos</p>'
        }
      }
    })
    expect(wrapper.html()).contains('htmlcontent="<p>my-tos</p>">')
  })
})

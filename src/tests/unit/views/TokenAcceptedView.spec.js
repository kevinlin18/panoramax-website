import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import TokenAcceptedView from '@/views/TokenAcceptedView.vue'
import i18n from '@/tests/unit/config'

describe('Template', () => {
  it('should render the page with the content', () => {
    const wrapper = shallowMount(TokenAcceptedView, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('pages.token_accepted.title')
    expect(wrapper.html()).contains('pages.token_accepted.text')
  })
})

import { it, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import EndUserLicenseAgreement from '@/views/EndUserLicenseAgreement.vue'
import i18n from '@/tests/unit/config'
vi.mock('axios', () => ({
  default: {
    get: vi.fn().mockResolvedValue({
      data: {
        languages: ['fr'],
        pages: ['end-user-license-agreement', 'terms-of-service']
      }
    }),
    defaults: { headers: { common: { 'Content-Type': 'text/html' } } }
  }
}))
describe('Template', () => {
  it('should have the default content', () => {
    const wrapper = shallowMount(EndUserLicenseAgreement, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg,
          content: '<p>my-tos</p>',
          authConf: {
            pages: ['end-user-license-agreement', 'terms-of-service']
          }
        }
      }
    })
    expect(wrapper.html()).contains('htmlcontent="<p>my-tos</p>">')
  })
})

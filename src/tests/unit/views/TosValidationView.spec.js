import { it, describe, expect, vi } from 'vitest'
import { shallowMount, flushPromises } from '@vue/test-utils'
import TosValidationView from '@/views/TosValidationView.vue'
import i18n from '@/tests/unit/config'
import axios from 'axios'

vi.mock('axios', () => ({
  default: {
    post: vi.fn().mockResolvedValue({}),
    get: vi.fn().mockResolvedValue({ data: { languages: ['fr'] } }),
    defaults: { headers: { common: { 'Content-Type': 'text/html' } } }
  }
}))
describe('Template', () => {
  it('should render the page with default the content', () => {
    const wrapper = shallowMount(TosValidationView, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg,
          content: '<p>my-tos</p>'
        }
      }
    })
    expect(wrapper.html()).contains('<p>my-tos</p>')
    expect(wrapper.html()).contains('pages.tos_validation.title')
    expect(wrapper.html()).contains('<input-checkbox')
    expect(wrapper.html()).contains('name="tos" label="" ischecked="false"')
    expect(wrapper.html()).contains(
      'for="tos" class="label">pages.tos_validation.tos_text</label>'
    )
    expect(wrapper.html()).contains('pages.tos_validation.summary_title')
    expect(wrapper.html()).contains(
      'disabled="true" isloading="false" text="pages.tos_validation.validation_button"'
    )
    expect(wrapper.html()).contains('<button')
  })
})
describe('Event', () => {
  describe('When the TOS are accepted', () => {
    it('should click and POST the accept_tos', async () => {
      const wrapper = shallowMount(TosValidationView, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg,
            content: '<p>my-tos</p>'
          }
        }
      })
      const checkbox = wrapper.findComponent({ name: 'InputCheckbox' })
      checkbox.vm.$emit('trigger')
      await flushPromises()
      expect(wrapper.vm.tosAccepted).toBe(true)
      expect(wrapper.html()).contains('disabled="false"')
      await wrapper.find('form').trigger('submit.prevent')
      await flushPromises()
      expect(axios.post).toHaveBeenCalledWith('api/users/me/accept_tos')
    })
  })
})

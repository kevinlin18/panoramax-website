import { it, describe, expect, vi } from 'vitest'
import { shallowMount, flushPromises } from '@vue/test-utils'
import MyStatsView from '@/views/MyStatsView.vue'
import { createI18n } from 'vue-i18n'
import axios from 'axios'
import fr from '@/locales/fr.json'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

const responseStats = {
  created: '2024-12-02T17:22:46.967334+00:00',
  description: 'List of all sequences of user elysee',
  extent: {
    spatial: {
      bbox: [
        [
          -1.7842280555555554, 43.81569419992222, 6.710469100002778,
          48.24853638888889
        ]
      ]
    },
    temporal: {
      interval: [
        ['2020-09-03T06:50:17+00:00', '2023-07-05T10:02:16.595000+00:00']
      ]
    }
  },
  'geovisio:length_km': 0.306,
  'geovisio:upload-software': 'unknown',
  id: 'user:7ec7ee1b-234f-4f05-9f6c-ca44a767ce32',
  keywords: ['pictures', "elysee's sequences"],
  license: 'proprietary',
  links: [
    {
      href: 'http://localhost:5000/api/',
      rel: 'parent',
      title: 'Instance catalog',
      type: 'application/json'
    },
    {
      href: 'http://localhost:5000/api/',
      rel: 'root',
      title: 'Instance catalog',
      type: 'application/json'
    },
    {
      href: 'http://localhost:5000/api/users/7ec7ee1b-234f-4f05-9f6c-ca44a767ce32/collection',
      rel: 'self',
      title: 'Metadata of this sequence',
      type: 'application/json'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [
              -1.7842280555555554, 48.24842138888889, -1.7840930555555554,
              48.24853638888889
            ]
          ]
        },
        temporal: {
          interval: [['2020-09-03T06:50:17+00:00', '2020-09-03T06:50:24+00:00']]
        }
      },
      'geovisio:length_km': 0.018,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/aa6e8e86-b1db-4800-bda5-f1975e6fa0da',
      id: 'aa6e8e86-b1db-4800-bda5-f1975e6fa0da',
      rel: 'child',
      'stats:items': {
        count: 5
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.095711+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [6.371482399944444, 43.871883099975, 6.371634599966666, 43.8720975]
          ]
        },
        temporal: {
          interval: [['2021-11-04T10:12:38+00:00', '2021-11-04T10:12:42+00:00']]
        }
      },
      'geovisio:length_km': 0.027,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/e1c9bd77-58ea-4d1a-b295-bad0191eb9a7',
      id: 'e1c9bd77-58ea-4d1a-b295-bad0191eb9a7',
      rel: 'child',
      'stats:items': {
        count: 3
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.202567+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [
              6.371536599975001, 43.87202299996111, 6.371536599975001,
              43.87202299996111
            ]
          ]
        },
        temporal: {
          interval: [['2021-11-04T10:07:44+00:00', '2021-11-04T10:07:44+00:00']]
        }
      },
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/c59c34b8-2f77-4f5f-82a1-26588b3ff6c7',
      id: 'c59c34b8-2f77-4f5f-82a1-26588b3ff6c7',
      rel: 'child',
      'stats:items': {
        count: 1
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.195096+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [6.541567299966666, 43.84386149996667, 6.542781599969422, 43.844076]
          ]
        },
        temporal: {
          interval: [['2021-12-14T15:01:34+00:00', '2021-12-14T15:01:40+00:00']]
        }
      },
      'geovisio:length_km': 0.102,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/b5fbb479-fe7d-48bf-8c79-6500fddb45bf',
      id: 'b5fbb479-fe7d-48bf-8c79-6500fddb45bf',
      rel: 'child',
      'stats:items': {
        count: 4
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.211072+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [
              6.710469100002778, 43.81569419992222, 6.710469100002778,
              43.81569419992222
            ]
          ]
        },
        temporal: {
          interval: [['2021-12-14T15:18:48+00:00', '2021-12-14T15:18:48+00:00']]
        }
      },
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/bb63d098-c5b9-402d-b627-bd3ea1f1f3ea',
      id: 'bb63d098-c5b9-402d-b627-bd3ea1f1f3ea',
      rel: 'child',
      'stats:items': {
        count: 1
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.229500+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [[4.81841, 45.74159, 4.8193399999999995, 45.74198]]
        },
        temporal: {
          interval: [
            [
              '2022-12-01T12:20:50.039000+00:00',
              '2022-12-01T12:21:14.373000+00:00'
            ]
          ]
        }
      },
      'geovisio:length_km': 0.009,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/a3bcf69b-29e2-46d1-a561-6b3600784971',
      id: 'a3bcf69b-29e2-46d1-a561-6b3600784971',
      rel: 'child',
      'stats:items': {
        count: 5
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.252545+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [
              6.591809399988816, 44.25666660000533, 6.592729199980617,
              44.25722689995621
            ]
          ]
        },
        temporal: {
          interval: [['2022-10-11T09:40:14+00:00', '2022-10-11T09:40:20+00:00']]
        }
      },
      'geovisio:length_km': 0.097,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/6b5c69b6-caf4-4849-8eb4-bc7e63278c76',
      id: '6b5c69b6-caf4-4849-8eb4-bc7e63278c76',
      rel: 'child',
      'stats:items': {
        count: 4
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.226048+00:00'
    },
    {
      created: '2024-12-02T17:22:46.967334+00:00',
      extent: {
        spatial: {
          bbox: [
            [6.676099600000001, 45.20433110000278, 6.676723, 45.20450530000278]
          ]
        },
        temporal: {
          interval: [
            [
              '2023-07-05T10:02:12.665000+00:00',
              '2023-07-05T10:02:16.595000+00:00'
            ]
          ]
        }
      },
      'geovisio:length_km': 0.053,
      'geovisio:status': 'ready',
      href: 'http://localhost:5000/api/collections/d86593b7-0b50-4d6b-b374-1266e3c7e071',
      id: 'd86593b7-0b50-4d6b-b374-1266e3c7e071',
      rel: 'child',
      'stats:items': {
        count: 4
      },
      title: 'Envoi du 2 décembre 2024, 18:22:43',
      updated: '2024-12-02T17:22:47.242686+00:00'
    }
  ],
  providers: [
    {
      id: '7ec7ee1b-234f-4f05-9f6c-ca44a767ce32',
      name: 'elysee',
      roles: ['producer']
    }
  ],
  stac_extensions: [
    'https://stac-extensions.github.io/stats/v0.2.0/schema.json',
    'https://stac.linz.govt.nz/v0.0.15/quality/schema.json',
    'https://stac-extensions.github.io/timestamps/v1.1.0/schema.json'
  ],
  stac_version: '1.0.0',
  'stats:items': {
    count: 27
  },
  title: "elysee's sequences",
  type: 'Collection',
  updated: '2024-12-02T17:22:47.252545+00:00'
}

describe('Template', () => {
  describe('When the stats list are fetched', () => {
    it('should render all the stats blocks', async () => {
      vi.spyOn(axios, 'get').mockResolvedValue({ data: responseStats })
      const wrapper = shallowMount(MyStatsView, {
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      await flushPromises()
      expect(wrapper.html()).contains('pages.stats.title')
      expect(axios.get).toHaveBeenCalledWith('api/users/me/collection')
      expect(wrapper.vm.userStats).toEqual([
        {
          count: 27,
          img: 'icon/picture-count.svg',
          text: 'photos',
          type: 'picture-count'
        },
        {
          count: 0.306,
          img: 'icon/picture-length.svg',
          text: 'km couverts',
          type: 'picture-length'
        },
        {
          count: 8,
          img: 'icon/collection-count.svg',
          text: 'séquences',
          type: 'collection-count'
        }
      ])
      expect(wrapper.html()).contains(
        'src="file:///assets/images/icon/picture-count.svg"'
      )
      expect(wrapper.html()).contains(
        'src="file:///assets/images/icon/picture-length.svg"'
      )
      expect(wrapper.html()).contains(
        'src="file:///assets/images/icon/collection-count.svg"'
      )
      expect(wrapper.html()).contains('27')
      expect(wrapper.html()).contains('0')
      expect(wrapper.html()).contains('8')
      expect(wrapper.html()).contains('photos')
      expect(wrapper.html()).contains('km couverts')
      expect(wrapper.html()).contains('séquences')
    })
  })
})

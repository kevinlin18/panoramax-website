import { it, describe, expect, vi } from 'vitest'
import {
  imageStatus,
  photoToDeleteOrPatchSelected,
  spliceIntoChunks,
  formatPaginationItems
} from '@/views/utils/sequence'
import { sortByName } from '@/views/utils/upload'
import {
  getAuthRoute,
  decodingFlaskCookie,
  sessionCookieDecoded
} from '@/utils/auth'
import { useCookies } from 'vue3-cookies'
vi.mock('vue3-cookies', () => {
  const mockCookies = {
    get: vi.fn(),
    remove: vi.fn(),
    keys: vi.fn()
  }
  return {
    useCookies: () => ({
      cookies: mockCookies
    })
  }
})
describe('imageStatus', () => {
  it('should render the "status" value', () => {
    const sequenceStatus = 'hidden'
    const imgStatus = 'not hidden'
    expect(imageStatus(imgStatus, sequenceStatus)).toEqual('hidden')
  })
  it('should render the "sequenceStatus" value', () => {
    const sequenceStatus = 'not hidden'
    const status = 'hidden'
    expect(imageStatus(status, sequenceStatus)).toEqual('hidden')
  })
})

describe('photoToDeleteOrPatchSelected', () => {
  it('should render true', () => {
    const imagesToDelete = ['1', '2']
    const item = {
      assets: { thumb: { href: '' }, hd: { href: '' } },
      properties: { created: new Date(), 'geovisio:status': '' },
      id: '1',
      bbox: [1, 3]
    }
    expect(photoToDeleteOrPatchSelected(item, imagesToDelete)).toEqual(true)
  })
  it('should render false', () => {
    const imagesToDelete = ['1', '2']
    const item = {
      assets: { thumb: { href: '' }, hd: { href: '' } },
      properties: { created: new Date(), 'geovisio:status': '' },
      id: '3',
      bbox: [1, 3]
    }
    expect(photoToDeleteOrPatchSelected(item, imagesToDelete)).toEqual(false)
  })
})

describe('spliceIntoChunks', () => {
  it('should render an chunked array of array with 4 elements max', () => {
    const array = ['123', '345', '6777', '0000', '66666', '222222', '9393888']
    const chunkSize = 4
    expect(spliceIntoChunks(array, chunkSize)).toEqual([
      ['123', '345', '6777', '0000'],
      ['66666', '222222', '9393888']
    ])
  })
})

describe('formatPaginationItems', () => {
  it('should render the "rel" links formated and without the "left" element', () => {
    const links = [
      {
        href: 'http://localhost:5000/api/',
        rel: 'root',
        title: 'Instance catalog',
        type: 'application/json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff5-4d70-88fa-6b2be3357709',
        rel: 'parent',
        type: 'application/json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff5-4d70-88fa-6b2be3357709/items?limit=100',
        rel: 'self',
        type: 'application/geo+json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff5-4d70-88fa-6b2be3357709/items?limit=100',
        rel: 'first',
        type: 'application/geo+json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff…a-6b2be3357709/items?limit=100&startAfterRank=100',
        rel: 'next',
        type: 'application/geo+json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff…-6b2be3357709/items?limit=100&startAfterRank=1023',
        rel: 'last',
        type: 'application/geo+json'
      }
    ]
    expect(formatPaginationItems(links)).toEqual([
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff5-4d70-88fa-6b2be3357709/items?limit=100',
        rel: 'double-left',
        type: 'application/geo+json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff…a-6b2be3357709/items?limit=100&startAfterRank=100',
        rel: 'right',
        type: 'application/geo+json'
      },
      {
        href: 'http://localhost:5000/api/collections/076e04c2-5ff…-6b2be3357709/items?limit=100&startAfterRank=1023',
        rel: 'double-right',
        type: 'application/geo+json'
      }
    ])
  })
})

describe('getAuthRoute', () => {
  it('should render auth route', () => {
    import.meta.env.VITE_API_URL = 'my-url/'
    const authRoute = 'auth'
    const nextRoute = 'mes-sequences'
    const returnedRoute = `${
      import.meta.env.VITE_API_URL
    }api/${authRoute}?next_url=${encodeURIComponent(
      `${location.protocol}//${location.host}${nextRoute}`
    )}`
    expect(getAuthRoute(authRoute, nextRoute)).toEqual(returnedRoute)
  })
})

describe('decodingFlaskCookie', () => {
  it('should return a decoded cookie', () => {
    const cookie =
      '.eJw9y0EKgzAQQNG7zLoDJpmYxMuUySRDra0pooUi3r3SRZcf_tuBRdo2rzDsMBYYgFxRytljkeyQrK0YVT16m6OhUlIihgvM_Kznfa88n9V4W2_XnzcuiqgmDBQMUtYec00WO3XqAndd7OUvXkt7j6Uup5vqRx6NJziOL8SPLNU.ZVy19Q.4DkVxu-LSF11uREkn6YIwHbn_0U'
    expect(decodingFlaskCookie(cookie)).toEqual(
      JSON.stringify({
        account: {
          id: '43df4bb5-dcb3-422e-8ff5-52b814dd994a',
          name: 'jean',
          oauth_id: '138ccff9-7471-4bf6-be92-0f3f37a0086c',
          oauth_provider: 'keycloak'
        }
      })
    )
  })
})
describe('sessionCookieDecoded', () => {
  it('should return a cookie parsed', () => {
    vi.spyOn(useCookies().cookies, 'get').mockReturnValue(
      '.eJw9y0EKgzAQQNG7zLoDJpmYxMuUySRDra0pooUi3r3SRZcf_tuBRdo2rzDsMBYYgFxRytljkeyQrK0YVT16m6OhUlIihgvM_Kznfa88n9V4W2_XnzcuiqgmDBQMUtYec00WO3XqAndd7OUvXkt7j6Uup5vqRx6NJziOL8SPLNU.ZVy19Q.4DkVxu-LSF11uREkn6YIwHbn_0U'
    )
    expect(sessionCookieDecoded()).toEqual({
      account: {
        id: '43df4bb5-dcb3-422e-8ff5-52b814dd994a',
        name: 'jean',
        oauth_id: '138ccff9-7471-4bf6-be92-0f3f37a0086c',
        oauth_provider: 'keycloak'
      }
    })
  })
  it('should return null', () => {
    vi.spyOn(useCookies().cookies, 'get').mockReturnValue(null)
    expect(sessionCookieDecoded()).toBe(null)
  })
})

describe('sortByName', () => {
  it('should return the the list sorted by name', () => {
    const list1 = [
      { name: 'd_1_ct.jpg' },
      { name: 'd_11_ct.jpg' },
      { name: 'd_2_ct.jpg' }
    ]
    expect(sortByName(list1)).toEqual([
      { name: 'd_1_ct.jpg' },
      { name: 'd_2_ct.jpg' },
      { name: 'd_11_ct.jpg' }
    ])

    const list2 = [{ name: 'A.jpg' }, { name: 'Z.jpg' }, { name: 'B.jpg' }]
    expect(sortByName(list2)).toEqual([
      { name: 'A.jpg' },
      { name: 'B.jpg' },
      { name: 'Z.jpg' }
    ])

    const list3 = [
      { name: 'CAM1_001.jpg' },
      { name: 'CAM2_002.jpg' },
      { name: 'CAM1_011.jpg' }
    ]
    expect(sortByName(list3)).toEqual([
      { name: 'CAM1_001.jpg' },
      { name: 'CAM1_011.jpg' },
      { name: 'CAM2_002.jpg' }
    ])
  })
})

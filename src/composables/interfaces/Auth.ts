export interface AuthConfigInterface {
  license?: { id: string; url: string }
  auth?: {
    enabled?: boolean
    user_profile?: { url?: string }
  }
}

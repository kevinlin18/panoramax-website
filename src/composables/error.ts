import { getAuthRoute } from '@/utils/auth'
import { useCookies } from 'vue3-cookies'
import { useSequenceStore } from '@/store/sequence'

export function manageRequestErrors(
  err: unknown,
  genError: string,
  path: string,
  authErr: boolean
): void {
  const { cookies } = useCookies()
  const sequenceStore = useSequenceStore()
  if (err?.response?.status === 401) {
    if (authErr) cookies.set('authError', 'true')
    else cookies.remove('authError')
    window.location.href = getAuthRoute('auth/login', path)
  } else sequenceStore.addToastText(genError, 'error')
}

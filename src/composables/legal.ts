import { onMounted, ref } from 'vue'
import axios from 'axios'
import { manageLocaleForPages } from '@/utils'
const content = ref<string>('')

export function legal(locale: string, props: { uri: string }) {
  onMounted(async () => {
    axios.defaults.headers.common['Content-Type'] = 'text/html'
    const { data } = await axios.get(`api/pages/${props.uri}`)
    const foundPage = await axios.get(
      `api/pages/${props.uri}/${await manageLocaleForPages(
        locale,
        data.languages
      )}`
    )
    if (foundPage?.data) content.value = foundPage.data
  })
  return { content }
}

import { computed, onMounted, onUnmounted, ref, watch } from 'vue'
import { useCookies } from 'vue3-cookies'
import { onBeforeRouteLeave, useRoute } from 'vue-router'
import type {
  FolderInterface,
  SequenceInterface,
  UploadErrorFilesInterface,
  UploadErrorInterface,
  UploadSetCreatedInterface
} from '@/views/interfaces/UploadPicturesView'
import {
  calcPercentage,
  checkFileExtension,
  countTotalFiles,
  formatSequenceTitle,
  isFolder,
  sortByName
} from '@/views/utils/upload'
import {
  deleteAnUploadSet,
  patchACollection
} from '@/views/utils/sequence/request'
import { manageRequestErrors } from '@/composables/error'
import {
  completeUploadSet,
  createAPictureToAnUploadSet,
  createUploadSets,
  fetchUploadSet,
  fetchUploadSetFiles
} from '@/views/utils/upload/request'
import md5 from 'js-md5'
const { cookies } = useCookies()
const uploadSets = ref<UploadSetCreatedInterface | null>(null)
const uploadSetForcedComplete = ref<boolean>(false)
const isLoading = ref<boolean>(false)
const isLoaded = ref<boolean>(false)
const noUploadText = ref<boolean>('')
const isUploadCanceled = ref<boolean>(false)
const uploadedSequence = ref<SequenceInterface | null>(null)
const newSequenceTitle = ref<string | null>(null)
const sequenceTitle = ref<string>(formatSequenceTitle())
const intervalId = ref<null | ReturnType<typeof setInterval>>(null)
const authError = ref<string | null>(null)
const informationCardDisplayed = ref<boolean>(true)
const folderUploading = ref<string | null>(null)
const editNewSequenceTitle = ref<string | null>(null)
const picturesUploadingSize = ref<number>(0)
const picturesToUploadSize = ref<number>(0)
const totalFiles = ref<number>(0)
const loadPercentage = ref<string>('0%')
const pictures = ref<File[] | []>([])
const isLoadingTitle = ref<boolean>(false)
const uploadError = ref<string | undefined>(undefined)
const allPictures = ref<File[] | FileList | FolderInterface[] | []>([])
const picturesCount = ref<number>(0)
const otherFilesCount = ref<number>(0)
const createAnUploadSet = ref<UploadSetCreatedInterface | null>(null)
const wakeLock = ref<Awaited<
  ReturnType<typeof navigator.wakeLock.request>
> | null>(null)

export function uploadComputed() {
  const clacNbItems = computed<number>(() => {
    if (uploadSets.value?.items_status) {
      return (
        uploadSets.value.items_status.prepared +
        uploadSets.value.items_status.broken +
        uploadSets.value.items_status.rejected +
        uploadSets.value.items_status.not_processed
      )
    }
    return 0
  })
  const progressBarPercentage = computed<number>(() => {
    if (uploadSetForcedComplete.value) return 100
    if (uploadSets.value?.nb_items) {
      return calcPercentage(
        clacNbItems.value,
        uploadSets.value.estimated_nb_files
      )
    }
    return 0
  })
  const errorPercentage = computed<number>(() => {
    if (uploadSets.value?.items_status) {
      const itemsError =
        uploadSets.value.items_status.broken +
        uploadSets.value.items_status.rejected
      return calcPercentage(itemsError, uploadSets.value.estimated_nb_files)
    }
    return 0
  })
  const progressClasse = computed<string>(() => {
    return progressBarPercentage.value !== 0 ? 'progress-bar-processing' : ''
  })
  const inputIsDisplayed = computed<boolean | null>(() => {
    return (
      !isLoading.value ||
      isLoaded.value ||
      (uploadedSequence.value && !uploadedSequence.value.pictures)
    )
  })
  return {
    clacNbItems,
    progressBarPercentage,
    errorPercentage,
    progressClasse,
    inputIsDisplayed
  }
}
export function setNewSequenceTitle(value: string | null): string | null {
  newSequenceTitle.value = value
}
export function commonVueHooks(confirmText: string) {
  watch([isLoading, isLoaded], () => {
    if (!isLoaded.value && isLoading.value) {
      window.onbeforeunload = function (e) {
        e = e || window.event
        if (e) e.returnValue = 'Sure?'
        return 'Sure?'
      }
    } else window.onbeforeunload = null
  })
  onBeforeRouteLeave((to, from, next) => {
    if (!isLoaded.value && isLoading.value) {
      const answer = window.confirm(confirmText)
      if (answer) return next()
      return next(false)
    }
    next()
  })
}
export function vueHookUpload(authErrText: string, seqTitle: string) {
  uploadSets.value = null
  uploadedSequence.value = null
  isLoading.value = false
  isLoaded.value = false
  onMounted(() => {
    if (cookies.get('authError')) {
      authError.value = authErrText
      cookies.remove('authError')
    }
    sequenceTitle.value = formatSequenceTitle(seqTitle)
    setInterval(() => {
      sequenceTitle.value = formatSequenceTitle(seqTitle)
    }, 1000)
  })
  onUnmounted(() => {
    window.onbeforeunload = null
    cookies.remove('authError')
    authError.value = null
    if (intervalId.value) clearInterval(intervalId.value)
  })
}
export function vueHookMyUpload() {
  isLoading.value = false
  isLoaded.value = false
  onMounted(async () => {
    const route = useRoute()
    const { data } = await fetchUploadSet(route.params.id)
    uploadSets.value = data
    await fetchAndFormatErrors(route.params.id)
  })
  onUnmounted(() => {
    window.onbeforeunload = null
    if (intervalId.value) clearInterval(intervalId.value)
  })
}
function formatPercentageAndPictures(
  pictureType: 'pictures' | 'picturesOnError',
  pictures: object,
  size: number
) {
  picturesUploadingSize.value += size
  if (uploadedSequence.value && uploadedSequence.value[pictureType]) {
    ;(uploadedSequence.value[pictureType] as unknown[]) = [
      ...(uploadedSequence.value[pictureType] as unknown[]),
      pictures
    ]
  }
  loadPercentage.value = `${calcPercentage(
    picturesUploadingSize.value,
    picturesToUploadSize.value
  )}%`
}
function picturesToUploadSizeText(): void {
  let fullSize = 0
  for (let i = 0; i < pictures.value.length; i++) {
    fullSize += pictures.value[i].size
  }
  picturesToUploadSize.value = fullSize
}
export async function updateSequenceTitle(
  value: string | null,
  id: string,
  errText: string
): Promise<void> {
  const route = useRoute()
  try {
    if (!value) return
    await patchACollection(id, { title: value })
    isLoadingTitle.value = false
    if (uploadSets.value?.associated_collections) {
      const foundItem = uploadSets.value.associated_collections.find(
        (item) => item.id === id
      )
      if (foundItem) foundItem.title = value
    }
  } catch (err) {
    manageRequestErrors(err, errText, route.path, true)
    isLoadingTitle.value = false
  }
}
export async function cancelUpload(answerText: string): Promise<void> {
  const answer = window.confirm(answerText)
  if (answer && createAnUploadSet.value?.id) {
    await deleteAnUploadSet(createAnUploadSet.value.id)
    pictures.value = []
    allPictures.value = []
    isLoaded.value = true
    isLoading.value = false
    picturesCount.value = 0
  }
}
function formatNewTitle(isFolder: boolean): string {
  let title = sequenceTitle.value
  if (isFolder) {
    const isOneFolder = allPictures.value.length === 1
    if (newSequenceTitle.value) title = newSequenceTitle.value
    else if (!newSequenceTitle.value && isOneFolder) {
      title = allPictures.value[0].folderName
      newSequenceTitle.value = title
    } else title = sequenceTitle.value
  } else {
    if (newSequenceTitle.value) title = newSequenceTitle.value
  }
  return title
}
async function formatBeforeAddCommon(
  value: File[] | FileList | FolderInterface[]
) {
  uploadSetForcedComplete.value = false
  try {
    wakeLock.value = await navigator.wakeLock.request('screen')
  } catch (err: unknown) {
    console.log(`${err.name}, ${err.message}`)
  }
  uploadedSequence.value = null
  cookies.remove('authError')
  authError.value = null
  isUploadCanceled.value = false
  if (value) allPictures.value = value
}
function formatTotalFiles() {
  if ((allPictures.value[0] as FolderInterface).folderName) {
    totalFiles.value = countTotalFiles(allPictures.value as FolderInterface[])
  } else totalFiles.value = allPictures.value.length
}
function formatAllPicturesWithExtension() {
  if (isFolder(allPictures.value)) {
    allPictures.value = allPictures.value.map((folder: FolderInterface) => ({
      ...folder,
      files: checkFileExtension(folder.files)
    }))
  }
}
async function formatAfterAddCommon(
  uploadSet: UploadSetCreatedInterface,
  errImg: string
) {
  if (isFolder(allPictures.value) && uploadSet) {
    for (const el of allPictures.value) {
      folderUploading.value = el.folderName
      formatBeforeUpload(el.files)
      await uploadPicture(uploadSet, sequenceTitle.value, errImg)
      allPictures.value = allPictures.value.filter(
        (e: FolderInterface) => e.folderName !== folderUploading.value
      )
    }
  } else if (!isFolder(allPictures.value) && uploadSet) {
    formatBeforeUpload(allPictures.value)
    await uploadPicture(uploadSet, uploadSet.title, errImg)
  }
  if (!uploadSet?.id) return
  const uploadSetFetched = await fetchUploadSet(uploadSet.id)
  if (!uploadSetFetched?.data?.completed) {
    uploadSetForcedComplete.value = true
    await completeUploadSet(uploadSet.id)
  }
}
export async function addMyPictures(
  value: File[] | FileList | FolderInterface[],
  errText: string,
  errImg: string,
  errNoUpload: string
): Promise<void> {
  await formatBeforeAddCommon(value)
  formatAllPicturesWithExtension()
  const comparedFilesToUpload = await compareMD5(
    uploadSets.value.id,
    allPictures.value
  )
  if (comparedFilesToUpload?.length) {
    formatTotalFiles()
    const uploadSet = uploadSets.value
    createAnUploadSet.value = uploadSet ? uploadSet : null
    await formatAfterAddCommon(uploadSet, errImg)
  } else {
    noUploadText.value = errNoUpload
    setTimeout(() => (noUploadText.value = ''), 5500)
  }
}
export async function addPictures(
  value: File[] | FileList | FolderInterface[],
  errText: string,
  errImg: string,
  routePath: string
): Promise<void> {
  await formatBeforeAddCommon(value)
  uploadSets.value = null
  formatAllPicturesWithExtension()
  formatTotalFiles()
  const uploadSet = await createUploadSet(
    formatNewTitle(isFolder(allPictures.value)),
    errText,
    routePath
  )
  createAnUploadSet.value = uploadSet ? uploadSet : null
  await formatAfterAddCommon(uploadSet, errImg)
  return {
    isLoading,
    isLoaded
  }
}
function formatBeforeUpload(value: File[] | FileList | FolderInterface[]) {
  const files = sortByName([].slice.call(value))
  const jpegFiles = checkFileExtension(files)
  const otherFiles = files.filter(
    (e) =>
      (e.type?.length > 0 && e.type !== 'image/jpeg') ||
      (e.type?.length > 0 && e.type !== 'image/jpg')
  )
  otherFilesCount.value = otherFiles.length
  pictures.value = jpegFiles
  picturesCount.value = pictures.value.length
  picturesUploadingSize.value = 0
  picturesToUploadSize.value = 0
  loadPercentage.value = '0%'
}
async function createUploadSet(
  title: string,
  errText: string,
  routePath: string
): Promise<UploadSetCreatedInterface | unknown> {
  try {
    let params = { title, estimated_nb_files: totalFiles.value }
    if (isFolder(allPictures.value)) {
      const meta = allPictures.value.map((e) => ({
        folderName: e.folderName,
        files: e.files.length
      }))
      params = { ...params, metadata: { folders: meta } }
    }
    const response = await createUploadSets(params)
    return response.data
  } catch (err: unknown) {
    manageRequestErrors(err, errText, routePath, true)
    isLoaded.value = true
  }
}
async function generateMD5(file: File): Promise {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = () => {
      const fileContent = reader.result
      const hash = md5(fileContent)
      resolve(hash)
    }
    reader.onerror = (error) => reject(error)
    reader.readAsArrayBuffer(file)
  })
}
function getUniqueObjects(arrOne, arrTwo) {
  const arrTwoMd5 = arrTwo.map((item) => item.content_md5)
  const filteredArray = arrOne.filter(
    (item) => !arrTwoMd5.includes(item.content_md5)
  )
  return filteredArray.map((e) => e.file)
}
async function formatMD5(files: File): Promise[] {
  for (const el of files) {
    return {
      file: el,
      content_md5: await generateMD5(el)
    }
  }
}
async function compareMD5(uploadsetId: string, files: File[]): Promise {
  const { data } = await fetchUploadSetFiles(uploadsetId)
  const arrayFiles = (el) =>
    Array.isArray(formatMD5(el)) ? formatMD5(el) : [formatMD5(el)]
  let formatFile = null
  if (!isFolder(files)) formatFile = arrayFiles(files)
  else formatFile = files.flatMap((folder) => arrayFiles(folder.files))
  return Promise.all(formatFile).then((values) =>
    getUniqueObjects(values, data.files)
  )
}
async function uploadPicture(
  uploadSet: UploadSetCreatedInterface,
  title: string,
  errImg: string
): Promise<void> {
  isLoaded.value = false
  isLoading.value = true
  if (!pictures.value || !pictures.value.length) {
    isLoaded.value = true
    isLoading.value = false
    return
  }
  picturesToUploadSizeText()
  const picturesToUpload = [...pictures.value]
  uploadedSequence.value = {
    title: title,
    id: uploadSet.id,
    pictures: [],
    picturesOnError: [],
    pictureCount: pictures.value.length,
    pictureSize: ''
  }
  for (const el of picturesToUpload) {
    if (pictures.value.length === 0) break
    const body = new FormData()
    body.append('file', el)
    try {
      const pictureUploaded = await createAPictureToAnUploadSet(
        uploadSet.id,
        body
      )
      if (pictures) {
        formatPercentageAndPictures('pictures', pictureUploaded, el.size)
      }
    } catch (err: unknown) {
      if (err.status != 409 && err.status != 400) uploadError.value = errImg
    }
  }
  await fetchAndFormatErrors(uploadSet.id)
  pictures.value = []
  isLoaded.value = true
  picturesCount.value = 0
  if (wakeLock.value) {
    wakeLock.value.release()
    wakeLock.value = null
  }
  launchInterval(uploadSet.id)
}
async function fetchAndFormatErrors(uploadSetId: string) {
  const { data } = await fetchUploadSetFiles(uploadSetId)
  const filteredErrors = data.files.filter(
    (e: UploadErrorFilesInterface) => e.rejected?.reason
  )
  let severities: string[] = []
  const formatError = filteredErrors.map((el: UploadErrorFilesInterface) => {
    if (!severities.includes(el.rejected.severity)) {
      severities = [...severities, el.rejected.severity]
    }
    let errP: UploadErrorInterface = {
      message: el.rejected.message,
      severity: el.rejected.severity,
      name: el.file_name,
      reason: el.rejected.reason
    }
    if (el?.rejected?.details) errP = { ...errP, details: el.rejected?.details }
    formatPercentageAndPictures('picturesOnError', errP, el.size)
    return errP
  })
  uploadedSequence.value = {
    ...uploadedSequence.value,
    title: uploadedSequence.value?.title || '',
    id: uploadedSequence.value?.id || '',
    pictures: uploadedSequence.value?.pictures || [],
    pictureCount: uploadedSequence.value?.pictureCount || 0,
    pictureSize: uploadedSequence.value?.pictureSize || '',
    picturesOnError: formatError
  }
  if (severities.includes('error') && severities.includes('info')) {
    const onlyErrors = uploadedSequence.value.picturesOnError.map((el) => ({
      ...el,
      severity: 'error'
    }))
    uploadedSequence.value = {
      ...uploadedSequence.value,
      picturesOnError: onlyErrors
    }
  }
}
function launchInterval(uploadSetId: string) {
  if (intervalId.value) return
  intervalId.value = setInterval(async () => {
    try {
      if (uploadSets.value?.dispatched) {
        await fetchAndFormatErrors(uploadSetId)
        if (intervalId.value) clearInterval(intervalId.value)
        intervalId.value = null
        return
      }
      const response = await fetchUploadSet(uploadSetId)
      uploadSets.value = response.data
    } catch (err: unknown) {
      console.log(err)
      if (intervalId.value) clearInterval(intervalId.value)
    }
  }, 1000)
}
export function getAllValues() {
  return {
    loadPercentage,
    newSequenceTitle,
    sequenceTitle,
    authError,
    picturesCount,
    totalFiles,
    allPictures,
    isLoading,
    isLoaded,
    isUploadCanceled,
    uploadError,
    informationCardDisplayed,
    uploadSetForcedComplete,
    uploadedSequence,
    folderUploading,
    editNewSequenceTitle,
    uploadSets,
    noUploadText
  }
}

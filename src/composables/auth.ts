import axios from 'axios'
import { onMounted, watchEffect, ref } from 'vue'
import { sessionCookieDecoded } from '@/utils/auth'
import type { AuthConfigInterface } from './interfaces/Auth'

export function authConfig() {
  const authConf = ref<AuthConfigInterface>()

  async function getConfig(): Promise<AuthConfigInterface> {
    const { data } = await axios.get('api/configuration', {
      withCredentials: false
    })
    return data
  }

  onMounted(async () => (authConf.value = await getConfig()))
  return { authConf }
}

export function isAuth() {
  const isLogged = ref<boolean>(false)
  watchEffect(() => {
    const cookie = sessionCookieDecoded()
    isLogged.value = !!(cookie && cookie.account)
  })
  return { isLogged }
}

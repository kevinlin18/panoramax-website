import { createApp } from 'vue'
import i18n from './i18n'
import VueMatomo from 'vue-matomo'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { useCookies } from 'vue3-cookies'
import { VueDraggableResizable } from 'vue-draggable-resizable-vue3'
import VCalendar from 'v-calendar'
import 'v-calendar/style.css'
import { pinia } from './store'
import './assets/main.scss'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import '@panoramax/web-viewer/build/index.css'
import { getEnv } from '@/utils'
const { cookies } = useCookies()
declare global {
  interface Window {
    _paq: unknown[]
  }
}

axios.defaults.baseURL = getEnv('VITE_API_URL')
axios.defaults.withCredentials = true
if (cookies.get('lang')) {
  axios.defaults.headers.common['Accept-Language'] = cookies.get('lang')
}
const matomoHost = getEnv('VITE_MATOMO_HOST')
const matomoSiteId = getEnv('VITE_MATOMO_SITE_ID')
const matomoExist = matomoHost && matomoSiteId

const app = createApp(App)
app.use(pinia)
app.use(i18n)
app.use(router)
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)
app.use(VueDraggableResizable)
app.use(VCalendar)
if (matomoExist) {
  app.use(VueMatomo, {
    host: matomoHost,
    siteId: matomoExist
  })
}
app.mount('#app')
if (matomoExist) {
  window._paq.push(['trackPageView']) // Pour suivre les visites sur vos pages
}

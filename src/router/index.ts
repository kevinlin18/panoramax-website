import { createRouter, createWebHistory } from 'vue-router'
import type {
  RouteRecordRaw,
  NavigationGuardNext,
  RouteLocationNormalized
} from 'vue-router'
import axios from 'axios'
import { getAuthRoute, sessionCookieDecoded } from '@/utils/auth'
import HomeView from '@/views/HomeView.vue'
import MyInformationView from '@/views/MyInformationView.vue'
import MyStatsView from '@/views/MyStatsView.vue'
import MySequencesView from '@/views/MySequencesView.vue'
import MySequenceView from '@/views/MySequenceView.vue'
import MyUploadSetView from '@/views/MyUploadSetView.vue'
import SharePicturesView from '@/views/SharePicturesView.vue'
import UploadPicturesView from '@/views/UploadPicturesView.vue'
import EndUserLicenseAgreement from '@/views/EndUserLicenseAgreement.vue'
import TokenAccepted from '@/views/TokenAcceptedView.vue'
import TermsOfService from '@/views/TermsOfService.vue'
import Ay11View from '@/views/Ay11View.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import TosValidationView from '@/views/TosValidationView.vue'

let routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/mes-informations',
    redirect: '/account'
  },
  {
    path: '/account',
    name: 'my-information',
    component: MyInformationView
  },
  {
    path: '/my-statistics',
    name: 'my-stats',
    component: MyStatsView
  },
  {
    path: '/mes-sequences',
    redirect: '/your-pictures'
  },
  {
    path: '/your-pictures',
    name: 'my-sequences',
    component: MySequencesView
  },
  { path: '/sequence/:id', name: 'sequence', component: MySequenceView },
  { path: '/upload/:id', name: 'upload-set', component: MyUploadSetView },
  {
    path: '/pourquoi-contribuer',
    redirect: '/why-contribute'
  },
  {
    path: '/why-contribute',
    name: 'why-contribute',
    component: SharePicturesView
  },
  {
    path: '/upload',
    name: 'upload-pictures',
    component: UploadPicturesView
  },
  {
    path: '/tos-validation',
    name: 'tos-validation',
    component: TosValidationView
  },
  {
    path: '/terms-of-service',
    name: 'terms-of-service',
    component: TermsOfService
  },
  {
    path: '/end-user-license-agreement',
    name: 'end-user-license-agreement',
    component: EndUserLicenseAgreement
  },
  {
    path: '/token-accepted',
    name: 'token-accepted',
    component: TokenAccepted
  },
  { path: '/:pathMatch(.*)*', component: NotFoundView }
]
if (window.location.href.includes('.ign.')) {
  routes = [
    ...routes,
    {
      path: '/accessibilite',
      redirect: '/accessibility'
    },
    {
      path: '/accessibility',
      name: 'ay11',
      component: Ay11View
    }
  ]
}
const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) return savedPosition
    else if (to.hash) return { el: to.hash, behavior: 'smooth' }
    else return { top: 0 }
  }
})
router.beforeResolve(
  async (
    to: RouteLocationNormalized,
    from: RouteLocationNormalized,
    next: NavigationGuardNext
  ) => {
    const siteLoggedRoutes =
      to.name === 'tos-validation' ||
      to.name === 'my-stats' ||
      to.name === 'my-sequences' ||
      to.name === 'upload-pictures'

    if (siteLoggedRoutes) {
      if (!isSiteLogged()) {
        goToLoginPage(to.path)
      } else return next()
    }
    if (to.name === 'my-information') {
      try {
        const keycloakLogout = await isKeycloakLogout()
        if (keycloakLogout.status >= 300 || !isSiteLogged()) {
          return goToLoginPage(to.path)
        } else return next()
      } catch (err) {
        console.log(err)
        return goToLoginPage(to.path)
      }
    }
    next()
  }
)

function isSiteLogged(): boolean {
  const cookie = sessionCookieDecoded()
  return !!(cookie && cookie.account)
}

async function isKeycloakLogout(): Promise<{ status: number }> {
  const loginUrl = `/api/users/me`
  return await axios.get(loginUrl)
}

function goToLoginPage(path: string): void {
  window.location.replace(getAuthRoute('auth/login', path))
}

export default router

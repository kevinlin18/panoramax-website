export const ACTIVE_LOCALES = [
  'fr',
  'en',
  'pt',
  'hu',
  'de',
  'zh_Hant',
  'es',
  'it',
  'da',
  'eo'
]

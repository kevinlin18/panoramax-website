export function img(name: string): string {
  return new URL(`../assets/images/${name}`, import.meta.url).toString()
}

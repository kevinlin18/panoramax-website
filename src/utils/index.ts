import { ACTIVE_LOCALES } from '@/locales'

function manageSlashUrl(): string {
  let apiUrl = getEnv('VITE_API_URL')
  if (apiUrl.charAt(apiUrl.length - 1) !== '/') apiUrl += '/'
  return apiUrl
}

function getEnv(value: string | undefined) {
  if (value) return import.meta.env[value]
  return null
}

async function importLocales() {
  const imports = ACTIVE_LOCALES.map((locale: string) =>
    import(`../locales/${locale}.json`).then((module) => ({
      [locale]: module.default
    }))
  )

  const locales = await Promise.all(imports)
  return Object.assign({}, ...locales)
}

async function manageLocaleForPages(locale: string, languages: [string] | []) {
  let defaultLocale = 'en'
  const localExist = languages.filter((e) => e.language === locale).length
  const enExist = languages.filter((e) => e.language === 'en').length
  if (!localExist && !enExist && languages.length) {
    defaultLocale = languages[0].language
  }
  if (localExist) defaultLocale = locale
  return defaultLocale
}

export { manageSlashUrl, getEnv, importLocales, manageLocaleForPages }

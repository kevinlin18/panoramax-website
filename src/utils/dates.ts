import moment from 'moment/min/moment-with-locales'
import i18n from '@/i18n'
const { locale } = i18n.global

function formatDate(date: Date | null | string, formatType: string): string {
  const formatDate = moment(date)
  if (
    ['ja', 'ko', 'zh_Hant', 'zh_Hans'].includes(locale.value) &&
    ['ll', 'LL'].includes(formatType)
  ) {
    formatType = 'L'
  }
  if (locale.value === 'fr' && formatType === 'HH:mm') {
    formatType = 'HH[h]mm'
  }
  return formatDate.format(formatType)
}

function durationCalc(duration1: Date, duration2: Date, type: string): number {
  const duration = moment.duration(moment(duration1).diff(moment(duration2)))
  if (type == 'hours') return duration.hours()
  if (type == 'minutes') return duration.minutes()
  return duration.seconds()
}

export { formatDate, durationCalc }

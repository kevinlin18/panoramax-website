import { defineStore } from 'pinia'

export const useSequenceStore = defineStore('sequence', {
  state: () => ({
    toastText: <string>'',
    toastLook: <string>'',
    picId: <string>''
  }),
  actions: {
    addToastText(text: string, look: string): void {
      this.toastText = text
      this.toastLook = look
      setTimeout(() => (this.toastText = ''), 5500)
    },
    addSequence(id: string): void {
      this.picId = id
    }
  }
})

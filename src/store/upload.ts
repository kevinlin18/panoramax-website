import { defineStore } from 'pinia'
import type { UploadSetCreatedInterface } from '@/views/interfaces/UploadPicturesView'
import { fetchUploadSets } from '@/views/utils/upload/request'

export const useUploadStore = defineStore('upload', {
  state: () => ({
    upload: <string>'',
    uploadSets: <UploadSetCreatedInterface[] | null>null
  }),
  actions: {
    async getUploadSets(params: string): void {
      const responseUploadSets = await fetchUploadSets(params)
      this.uploadSets = responseUploadSets.data.upload_sets
    },
    addUpload(text: string): void {
      this.upload = text
    }
  }
})

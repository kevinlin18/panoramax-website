describe('In the login page', () => {
  it('type in the form to login', () => {
    cy.visit(`${Cypress.env('api_url')}api/auth/login`)
    cy.get('#username').type('Elysee')
    cy.get('#password').type('my password')
    cy.contains('Sign In').click()
    cy.visit('/')
  })
  it('go to the register form and create an account', () => {
    cy.visit(`${Cypress.env('api_url')}api/auth/login`)
    cy.fixture('login').then((loginData) => {
      cy.contains(loginData.textLinkRegister).click()
      cy.get('#firstName').type('Tom')
      cy.get('#lastName').type('Tom')
      cy.get('#email').type('test@test123.com')
      cy.get('#username').type('Elysee12445')
      cy.get('#password').type('my password1')
      cy.get('#password-confirm').type('my password1')
      cy.get('form').submit()
      cy.visit('/')
    })
  })
})

export {}

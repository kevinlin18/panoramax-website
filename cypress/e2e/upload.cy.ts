describe('In the login page', () => {
  it('login and go to the upload page to upload images', () => {
    cy.visit(`${Cypress.env('api_url')}api/auth/login`)
    cy.get('#username').type('Elysee')
    cy.get('#password').type('my password')
    cy.fixture('upload').then((uploadData: uploadInterface) => {
      cy.contains(uploadData.textLinkLogin).click()
      cy.visit('/upload')
    })
  })
})
interface uploadInterface {
  textLinkLogin: string
  textLinkUpload: string
  textButtonUpload: string
  textTitle: string
  textButtonTitle: string
}
export {}

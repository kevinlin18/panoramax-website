describe('In the contribute page', () => {
  it('go to the doc pages', () => {
    cy.visit('pourquoi-contribuer')
    cy.fixture('contribute').then((contributeData: contributeInterface) => {
      cy.get('.upload-text').scrollIntoView()
      cy.contains(contributeData.textButtonDocPython).click()
      cy.contains(contributeData.textButtonCli).click()
      cy.contains(contributeData.textButtonDocCli).click()
      cy.contains(contributeData.textButtonTiles).click()
      cy.contains(contributeData.textButtonDoc).click()
    })
  })
})
interface contributeInterface {
  textButtonContribute: string
  textButtonDocPython: string
  textButtonCli: string
  textButtonDocCli: string
  textButtonTiles: string
  textButtonDoc: string
}
export {}

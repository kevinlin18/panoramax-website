const express = require('express')

const app = express()
const directory = '/' + (process.env.STATIC_DIR || 'dist')
app.use(express.static(__dirname + directory))

const port = process.env.PORT || 3003
app.listen(port, function () {
  console.log('Listening on', port)
})
app.get(/(.*)/, function (request, response) {
  response.sendFile(__dirname + '/dist/index.html')
})

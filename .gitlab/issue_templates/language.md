# New language issue
*Hello 👋 Thank you for submitting an issue.*

*Before you start, please make sure your issue is understandable.*
*To make your issue readable make sure you use valid Markdown syntax.*

*https://guides.github.com/features/mastering-markdown/*

## Add a new language

The translation files are included from Weblate before each release.

For the first integration, the language translation needs to be 100% achieved.

Please specify the language code expected ([see the full list](https://github.com/WeblateOrg/language-data/blob/main/languages.csv)). We will manually make it available for you in Weblate.
If you are considering adding a country-specific variant of a language (e.g. de-AT), first make sure that the main language is well maintained (e.g. de). Your contribution might be useful to more people if you contribute to the existing version of your language rather than the country-specific variant.

**Language to add:**

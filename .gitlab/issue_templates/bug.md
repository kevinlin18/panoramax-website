# Bug report issue
*Hello 👋 Thank you for submitting an issue.*

*Before you start, please make sure your issue is understandable and reproducible.*
*To make your issue readable make sure you use valid Markdown syntax.*

*https://guides.github.com/features/mastering-markdown/*

## Bug description
*Describe the problem with as much detail as possible and a clear and concise description of what you expected to happen.*

## Steps to reproduce
*Describe with details the steps to reproduce the bug and the type of device on which the bug occurred (desktop, mobile, tablet etc)*\
*1. Go to '...'*\
*2. Click on '....'*\
*3. Scroll down to '....'*\
*4. See error*

## Technical specifications
*Write the technical specifications about the environnement when the bug occurred like :*\
*- Operating system:*\
*- OS version:*\
*- Browser:*\
*- Browser version:*\
*- Other technical details:*

### Screenshots
*If applicable, add screenshots to help explain your problem.*

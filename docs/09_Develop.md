# Work on the code

## Architecture

The website relies on the following technologies and components:

- Frontend website made in [Vue 3](https://vuejs.org/guide/introduction.html)
- Project use [Vite](https://vitejs.dev/guide/) who offer a fast development server and an optimized compilation for production (like webpack)
- The style is made with CSS/SASS and the [bootstrap library](https://getbootstrap.com/)
- [Typescript](https://www.typescriptlang.org/) used to type
- [Jest](https://jestjs.io/fr/) used for unit testing

## Compile and Hot-Reload for Development

Launch your dev server :

```sh
npm run dev
```

or

```sh
yarn dev
```

## Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

or

```sh
yarn test:unit
```

## Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

or

```sh
yarn lint
```

## Documentation

Documentation is located `docs` folder, and can be served with [Mkdocs](https://www.mkdocs.org/):

```bash
python -m venv env
source ./env/bin/activate
pip install mkdocs-material
mkdocs serve
```

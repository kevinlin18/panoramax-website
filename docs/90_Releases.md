# Make a release

The website uses [semantic versioning](https://semver.org/) for its release numbers.

!!! note
	Make sure that versions are in-sync with other Panoramax components. Each component can have different `PATCH` versions, but compatibility __must__ be ensured between `MAJOR.MINOR` versions.

Run these commands in order to issue a new release:

```bash
git checkout develop

vim package.json	# Change version
npm run doc

vim CHANGELOG.md	# Replace unreleased to version number and update versions links (at bottom)

git add *
git commit -m "Release x.x.x"
git tag -a x.x.x -m "Release x.x.x"
git push origin develop
git checkout main
git merge develop
git push origin main --tags
```

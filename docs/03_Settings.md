# Settings

Many things can be customized in your Panoramax instance Website.

## 1 - Basic Settings

Low-level settings can be configured using the `.env` file. You can find an example in the `env.example` file.

**Available Parameters**:

### General Configuration

- **`VITE_API_URL`**

  - **Description**: The URL to the Panoramax API.
  - **Expected Value**: A valid URL.
  - **Example**: `https://panoramax.ign.fr/`

- **`VITE_INSTANCE_NAME`**

  - **Description**: The name of your Panoramax instance.
  - **Expected Value**: A string representing the instance name.
  - **Example**: `IGN`

- **`VITE_ENV`**
  - **Description**: Specifies the environment (e.g., development or production).
  - **Expected Value**: A string, typically `dev` or `prod`.
  - **Example**: `dev`

### Map Configuration

- **`VITE_TILES`**

  - **Description**: URL of the vector tiles to use as the default map view.
  - **Expected Value**: a JSON string or a valid URL to a JSON file which has the vector tiles configuration
  - **Default**: Uses OpenStreetMap tiles (hosted by OpenStreetMap France) if not specified.
  - **Example**: `https://data.geopf.fr/annexes/ressources/vectorTiles/styles/PLAN.IGN/standard.json`

- **`VITE_MAP_ATTRIBUTION`**

  - **Description**: Text attribution to display inside the map viewer, associated with the tiles used.
  - **Expected Value**: A string.
  - **Example**: `© IGN`

- **`VITE_RASTER_TILE`**

  - **Description**: URL of the raster (aerial) tiles to use. Refer to [the MapLibre docs](https://maplibre.org/maplibre-style-spec/sources/#raster) for more information.
  - **Expected Value**: a JSON string or a valid URL to a JSON file which has the vector tiles configuration
  - **Example**: _(LEAVING THIS BLANK FOR NOW, DON'T MERGE. It's rare to have a url you can use for the aerial imagery but specifying json in a docker compose file is a nightmare)_

- **`VITE_ZOOM`**

  - **Description**: The initial zoom level of the map when it loads.
  - **Expected Value**: A number between `0` and `24`.
  - **Explanation**: `0` represents a fully zoomed-out view (world), while `24` is the maximum zoom level, displaying a close-up view.
  - **Default**: `0`

- **`VITE_MAX_ZOOM`**

  - **Description**: The maximum zoom level allowed on the map.
  - **Expected Value**: A number between `0` and `24`, but typically set to number at the higher end like `24`.
  - **Default**: `24`

- **`VITE_CENTER`**
  - **Description**: The center position for the map when it's first initialized.
  - **Expected Value**: A pair of coordinates in the format `Longitude,Latitude` (e.g., `0.000,0.000`).
  - **Explanation**: Currently, due to a known bug, only the digits before the decimal point are used, ignoring anything after the dot.

### Viewer and Metadata Configuration

- **`VITE_TITLE`**

  - **Description**: The title for the `<title>` tag of the HTML.
  - **Expected Value**: A string.
  - **Example**: `Panoramax IGN: photo-cartographier les territoires`

- **`VITE_META_DESCRIPTION`**

  - **Description**: The description for meta tags, which is useful for SEO.
  - **Expected Value**: A string.
  - **Example**: `L'instance Panoramax IGN permet la publication de photos de terrain pour cartographier le territoire. Panoramax favorise la réutilisation des photos pour de nombreux cas d'usage.`

- **`VITE_META_TITLE`**
  - **Description**: The title for the meta tags.
  - **Expected Value**: A string.
  - **Example**: `Panoramax IGN: photo-cartographier les territoires`


## 2 - Wording customization

Panoramax instance website can be customized to have wording reflecting your brand, licence and other elements.

All the wordings of the website are on this [locale file](https://gitlab.com/panoramax/server/website/-/blob/develop/src/locales/en.json). In there, you might want to change:

- The website title (properties `title` and `meta.title`)
- The description (property `meta.description`)
- Links to help pages:
  - `upload.description`
  - `upload.footer_description_terminal`

### 2.1 - Lang customization

There is already files to have custom langage li French and English [here](https://gitlab.com/panoramax/server/website/-/blob/develop/src/locales)
You can edit these files if you want change the wordings or use [the collaborative site Weblate](https://weblate.panoramax.xyz/)
If you want to add a new translation file, you can add it with the structure name `{locale}.json` and put it inside the folder.
After that you need to edit the `index.ts` and add your new locale [here](https://gitlab.com/panoramax/server/website/-/tree/develop/src/config/locales.ts)


## 3 - Visuals

The following images can be changed to make the website more personal:

- Logo: [`src/assets/images/logo.jpeg`](https://gitlab.com/panoramax/server/website/-/blob/develop/src/assets/images/logo.jpeg)
- Favicon: [`static/favicon.ico`](https://gitlab.com/panoramax/server/website/-/blob/develop/static/favicon.ico)

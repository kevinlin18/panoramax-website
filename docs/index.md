# Panoramax instance Website

Welcome to Panoramax __Instance Website__ documentation ! It will help you through all phases of setup, run and develop on Panoramax instance Website.

The website offers these functionalities:

- Display of pictures and their location (using the embed [web viewer](https://gitlab.com/panoramax/clients/web-viewer))
- Handle user authentication and account management
- Show simple to read documentation

!!! note
	The 📷 __web viewer__ (component showing pictures and their location on a map) is in [a separate, dedicated repository](https://gitlab.com/panoramax/clients/web-viewer). If you're looking for docs on another component, you may go to [this page](https://gitlab.com/panoramax) instead.

!!! note
	Also note that the __📖 presentation website panoramax.fr__ has [its own dedicated repository](https://gitlab.com/panoramax/server/panoramax.fr-website).

!!! note
	If at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/panoramax/server/website/-/issues) or by [email](mailto:panieravide@riseup.net).

# ![Panoramax](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/40px-Panoramax.svg.png) Panoramax

__Panoramax__ is a digital resource for sharing and using 📍📷 field photos. Anyone can take photographs of places visible from the public streets and contribute them to the Panoramax database. This data is then freely accessible and reusable by all. More information available at [gitlab.com/panoramax](https://gitlab.com/panoramax) and [panoramax.fr](https://panoramax.fr/).


# 💻 Panoramax Instance Website

This repository only contains __the web front-end for a Panoramax Instance__.

Note that the 📷 __web viewer__ (component showing pictures and their location on a map) is in [a separate, dedicated repository](https://gitlab.com/panoramax/clients/web-viewer).

Also note that the __📖 presentation website panoramax.fr__ has [its own dedicated repository](https://gitlab.com/panoramax/server/panoramax.fr-website).

## ⚙️ Features

The instance website offers these functionalities:

- Display of pictures and their location (using the embed [web viewer](https://gitlab.com/panoramax/clients/web-viewer))
- Handle user authentication and account management
- Show simple to read documentation

## 🕮 Documentation

[A full documentation](./docs/) is available to help you through the install, setup and usage of the Panoramax instance website.

## 💁 Contributing

Pull requests are welcome. For major changes, please open an [issue](https://gitlab.com/panoramax/server/website/-/issues) first to discuss what you would like to change.

## ⚖️ License

Copyright (c) Panoramax team 2022-2024, [released under MIT license](./LICENSE).

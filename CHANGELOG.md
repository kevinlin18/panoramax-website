# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Before _0.1.0_, website development was on rolling release, meaning there are no version tags.

## [2.6.12] - 2025-02-10

### Changed

- harmonization for the langague flags with Wikipedia logos
- update the viewer version to 3.2.3
- add Terms of Services management
  - display the content of all legal pages fetched with the api routes
  - manage the translated pages
  - manage the non accepted TOS to force the user to accept the TOS on the login and the register

### Fixed

- fix when the uploadSet creation is waiting

## [2.6.11] - 2025-01-17

### Changed

- add a message in the upload interface when the sequence creation is very long
- add a banner in the sequence list page when the sequence creation is very long

## [2.6.10] - 2025-01-16

### Changed

- add new locale languages : DA, IT
- add a new page interface to restart an upload if there were some error during an upload
- add a notification system to tell to the user if some uploads are on error
- add the button "Share pictures" on the logout mode
- add some gitlab templates issues
- some UI/UX changes

### Fixed

- fix warning when deploying the site
- fix the datetime format for the dates
- fix the account management for non keycloak instances
- fix the position of the CSV button in the sequence page
- fix the stats user page


## [2.6.9] - 2024-12-05

### Changed

- Add a link between the viewer and the management sequence page
- Add user stats page and reformat Mt account page
- Autocomplete uploadSet when there is a problem during upload
- Some fixs

## [2.6.8] - 2024-11-29

### Changed

- Update @panoramax/web-viewer to the version 3.2

## [2.6.7] - 2024-11-18

### Changed

- Add the possibility to set a title on an upload set when pictures are uploaded
- Add a better visualization about the errors after an upload of pictures
- Some documentation changes
- Remove the possibility to sort in the sequence page when there is only one picture on the sequence
- Add a wakelock to not set the screen in the saver mode when there is an upload sending
- Add the possibility to see the distance in km in the sequence list

### Fixed

- Update the TOS wordings for the IGN instance
- Fix the bug : html displayed when TOS not exist
- Some css fixes

## [2.6.6] - 2024-10-16

### Fixed

- Update the Viewer to the 3.1.1-develop-7d991845 version to fix IGN tiles bug

## [2.6.5] - 2024-10-16

### Changed

- Docker images are now published as `panoramax/website` on [dockerhub](https://hub.docker.com/r/panoramax/website)
- Update the Viewer to the 3.1.1 version
- Add a tooltip to the button sequence link
- translate urls to english
- the lang menu can be closed when there is a click anywhere in outside the menu

### Fixed

- fix Maj button for multiple select picture
- fix a bug about the filters in the sequence liste page
- fix about the fallback lang setup
- fix about reloading the home page when we are in the homepage
- fix about a problem in the sequence orientation if an image is not selected

## [2.6.4] - 2024-10-03

### Changed

- Update the Viewer to 3.1.0
- New upload page :
  - possibility tu upload a list of files : iso old upload page
  - add the possibility to drag/drop one or many folder with images inside
  - best error management
  - treatments in pictures (blurring, sequences creations etc) are displayed
  - list the uploaded sequences
- Sequence list page :
  - add a block in the page to see the uploads not finished
  - add the possibility to delete the uploads not finished
- On sequence page :
  - add a special view if the sequence does not exist
  - add a button on each picture to copy the url of the picture
- add a user friendly block to display first and last photo date in a sequence
- remove some useless elements in the logout mode
- add some new locales : es, zh
- add a 404 page
- best global error management

### Fixed

- fix some css in the site

## [2.6.3] - 2024-06-26

### Changed

- Update Geovisio Viewer to 3.0.2
- Add a new Header, and specifically on mobile
- Add TOS Page and TOS management :
  - management with multiples files and lang
  - management with env var

### Fixed

- Multiple fixes for UI/UX improvement :
  - fix error on upload img
  - fix sequence status inside a sequence
  - fix for cookie auth management
  - fix Page of a sequence : panel and button not accessible if not owner
- Fix tests unit

## [2.6.2] - 2024-05-15

### Changed

- Change Nodejs version to LTS 20.9.0
- Update dependencies (not prettier/typescript/eslint)
- Add tutorial on Android mobile for the picture upload
- Add tutorial on Android mobile when there is error on exif geoloc tag for the picture upload
- Add a server.js to serve statics files on deploy

### Fixed

- Fix orientation viewer on mobile
- Fix Header css
- Fix tests unit

## [2.6.1] - 2024-05-03

### Fixed

- Fix warning in browser console
- Fix lang switcher using cookie to set the locale

## [2.6.0] - 2024-05-02

### Changed

- Update Geovisio Viewer to 3.0.1
  - Adapt the code for the new viewer version
- Change some features after user testing:
  - Upload page tooltip
  - Add an upload cancel button
  - New mobile sequence list version
  - Add a return home button in the header
  - Improve the interaction in the Sequence page
- Improve the performance of the code
- Some refactoring
- Some bug fixes

## [2.5.1] - 2024-03-19

### Added

- Panel management to edit a sequence:
  - 3 tabs to edit in a sequence page
  - add the possibility to re-orient all the pictures of a sequence with a widget
  - add the possibility to sort the pictures of a sequence

## [2.5.0] - 2024-03-11

### Changed

- GeoVisio web viewer updated to [2.5.0](https://gitlab.com/panoramax/clients/web-viewer/-/compare/2.4.0...2.5.0) to reduce tiles size.

## [2.4.1] - 2024-02-01

### Fixed

- Fix geovisio version yarn.lock

## [2.4.0] - 2024-01-31

### Added

- Possibility to edit a sequence title in the sequence page

### Changed

- GeoVisio web viewer updated to [2.4.0](https://gitlab.com/panoramax/clients/web-viewer/-/compare/2.3.1...2.4.0) to manage sequence by user

### Fixed

- Fix filter reset button to include bbox filter
- Fix fullscreen button added by the widget viewer
- Some UI and UX fixes before user tests

## [2.3.1] - 2024-01-29

### Added

- Add the possibility to fullscreen the viewer in the homepage : https://gitlab.com/panoramax/server/website/-/issues/60
- In the sequence list page add a filter by bbox in the map : https://gitlab.com/panoramax/server/website/-/issues/61
- In the sequence list page add a filter by date in the list : https://gitlab.com/panoramax/server/website/-/issues/57
- Add a cancel button to when a sequence is uploading to cancel the upload

### Changed

- GeoVisio web viewer updated to [2.3.1](https://gitlab.com/panoramax/clients/web-viewer/-/compare/2.3.0...2.3.1)

### Fixed

- Some UI and UX fixes before user tests

## [2.3.0] - 2023-12-06

### Added

- Add the possibility to an user to select a sequence in the list using the map : https://gitlab.com/panoramax/server/website/-/merge_requests/100
- For a selected sequence in the list, if the sequence is not displayed in the map, fly to the sequence on the map : https://gitlab.com/panoramax/server/website/-/merge_requests/108
- Add the pagination to the sequence with sort with API routes : https://gitlab.com/panoramax/server/website/-/merge_requests/107
- Add Hungarian translation : https://gitlab.com/panoramax/server/website/-/merge_requests/105
- Add the possibility to hide/delete a sequence in the sequence list : https://gitlab.com/panoramax/server/website/-/merge_requests/101
- Add the possibility for the user to change the title of the sequence before upload the pictures : https://gitlab.com/panoramax/server/website/-/merge_requests/101
- Add the possibility to resize the sequence page by dragging the blocs : https://gitlab.com/panoramax/server/website/-/merge_requests/101

### Changed

- GeoVisio web viewer updated to [2.3.0](https://gitlab.com/panoramax/clients/web-viewer/-/compare/2.2.1...2.3.0)

### Fixed

- Nginx server in Docker container was not recognizing routes other than `/` on first loading.
- Fix the cookie bug by decoding flask cookie : https://gitlab.com/panoramax/server/website/-/merge_requests/102

## [2.2.3] - 2023-11-03

### Added

- Add translation based on the browser language (only trad for FR and EN for now)

### Changed

- Page My Sequences, add the possibility to select a sequence in the list with the map :

  - the user can only see their sequences on the list
  - display the selected sequence in blue in the map
  - display a thumbnail the hovered sequence
  - hover the sequence selected in the map on the list

- add some test e2e
- maj viewer Geovisio version to 2.2.1
- fix some CSS

## [2.2.2] - 2023-10-16

### Changed

- fix lazy loading on imgs
- fix css to the license link

## [2.2.1] - 2023-10-16

### Changed

- fix some wordings
- licence text in bold
- remove footer when logged
- add tiles custom to the sequence page list

## [2.2.0] - 2023-10-13

### Added

- New UI/UX version :
  - new footer
  - new header
  - add a map to the sequence list page + the uploaded date
  - new wordings

### Changed

- Hide buttons delete and hide sequences images when you are not the owner

## [2.1.3] - 2023-09-25

### Changed

- fix the format date from YY to YYYY in a sequence page

### Added

- Add a first version of a footer
- Add a ay11 page

## [2.1.2] - 2023-09-12

### Changed

- add a fix to center the map with ENV variables

## [2.1.1] - 2023-09-06

### Changed

- GeoVisio web viewer upgraded to 2.1.4, [with alls its changes embedded](https://gitlab.com/panoramax/clients/web-viewer/-/blob/develop/CHANGELOG.md?ref_type=heads#213-2023-08-30).
- Dockerfile creates smaller and faster containers, using pre-built website and Nginx for HTTP serving.
- In the upload input you can now choose between gallery and camera on mobile IOS
- Some CSS fix for the responsive of one Sequence
- Insert the report button in the viewer by passing a params

### Added

- Get the License with the API route
- Add ENV var for maxZoom params of the viewer

## [2.1.0] - 2023-08-29

### Added

- A new page `/envoyer` to upload picture with an interface ([#13](https://gitlab.com/panoramax/server/website/-/issues/13)) :
  - the user can upload multiples pictures with the interface
  - the pictures are sorted by name
  - the user can see all the pictures uploaded and all the errors

### Changed

- Website releases now follow the synced `MAJOR.MINOR` API version rule, meaning that any version >= 2.1 of the website will be compatible with corresponding [GeoVisio API](https://gitlab.com/panoramax/server/api) version.

### Fixed

- fix a bug in the header hidden sub menu when authentication is not with keycloak

## [0.1.0] - 2023-07-04

### Added

- A new page `/mes-sequences` to access to a list of sequences for a logged user ([#14](https://gitlab.com/panoramax/server/website/-/issues/14)) :

  - the user can see all their sequences
  - the user can filter sequences
  - the user can enter to a specific sequence

- A new page `/sequence/:id` to access to a sequence of photos for a logged user ([#14](https://gitlab.com/panoramax/server/website/-/issues/14)) :
  - the user can see the sequence on the map and move on the map from photos to photos
  - the user can see information about the sequence
  - the user can see all the sequence's photos
  - the user can disable and delete one or many photo(s) of the sequence

### Changed

- Header have now a new entry `Mes photos` when the user is logged to access to the sequence list
- The router guard for logged pages has been changed to not call the api to check the token

[unreleased]: https://gitlab.com/panoramax/server/website/-/compare/2.6.12...develop
[2.6.11]: https://gitlab.com/panoramax/server/website/-/compare/2.6.11...2.6.12
[2.6.11]: https://gitlab.com/panoramax/server/website/-/compare/2.6.10...2.6.11
[2.6.10]: https://gitlab.com/panoramax/server/website/-/compare/2.6.9...2.6.10
[2.6.9]: https://gitlab.com/panoramax/server/website/-/compare/2.6.8...2.6.9
[2.6.8]: https://gitlab.com/panoramax/server/website/-/compare/2.6.7...2.6.8
[2.6.7]: https://gitlab.com/panoramax/server/website/-/compare/2.6.6...2.6.7
[2.6.6]: https://gitlab.com/panoramax/server/website/-/compare/2.6.5...2.6.6
[2.6.5]: https://gitlab.com/panoramax/server/website/-/compare/2.6.4...2.6.5
[2.6.4]: https://gitlab.com/panoramax/server/website/-/compare/2.6.3...2.6.4
[2.6.3]: https://gitlab.com/panoramax/server/website/-/compare/2.6.2...2.6.3
[2.6.2]: https://gitlab.com/panoramax/server/website/-/compare/2.6.1...2.6.2
[2.6.1]: https://gitlab.com/panoramax/server/website/-/compare/2.6.0...2.6.1
[2.6.0]: https://gitlab.com/panoramax/server/website/-/compare/2.5.1...2.6.0
[2.5.1]: https://gitlab.com/panoramax/server/website/-/compare/2.5.0...2.5.1
[2.5.0]: https://gitlab.com/panoramax/server/website/-/compare/2.4.1...2.5.0
[2.4.1]: https://gitlab.com/panoramax/server/website/-/compare/2.4.0...2.4.1
[2.4.0]: https://gitlab.com/panoramax/server/website/-/compare/2.3.1...2.4.0
[2.3.1]: https://gitlab.com/panoramax/server/website/-/compare/2.3.0...2.3.1
[2.3.0]: https://gitlab.com/panoramax/server/website/-/compare/2.2.3...2.3.0
[2.2.3]: https://gitlab.com/panoramax/server/website/-/compare/2.2.2...2.2.3
[2.2.2]: https://gitlab.com/panoramax/server/website/-/compare/2.2.1...2.2.2
[2.2.1]: https://gitlab.com/panoramax/server/website/-/compare/2.2.0...2.2.1
[2.2.0]: https://gitlab.com/panoramax/server/website/-/compare/2.1.3...2.2.0
[2.1.3]: https://gitlab.com/panoramax/server/website/-/compare/2.1.2...2.1.3
[2.1.2]: https://gitlab.com/panoramax/server/website/-/compare/2.1.1...2.1.2
[2.1.1]: https://gitlab.com/panoramax/server/website/-/compare/2.1.0...2.1.1
[2.1.0]: https://gitlab.com/panoramax/server/website/-/compare/0.1.0...2.1.0
[0.1.0]: https://gitlab.com/panoramax/server/website/-/commits/0.1.0

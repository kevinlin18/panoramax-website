#!/bin/bash

ROOT_DIR=/usr/share/nginx/html
DOCKER_VARS=(VITE_INSTANCE_NAME VITE_API_URL VITE_TILES VITE_MAX_ZOOM VITE_ZOOM VITE_CENTER VITE_RASTER_TILE VITE_TITLE VITE_META_DESCRIPTION VITE_META_TITLE VITE_MAP_ATTRIBUTION)

echo "Setting env variables in web files"
for file in $ROOT_DIR/assets/*.js $ROOT_DIR/*.html; do
	echo "Processing $file...";

	for i in ${!DOCKER_VARS[@]}; do
		sed -i "s|DOCKER_${DOCKER_VARS[i]}|${!DOCKER_VARS[i]}|g" $file
	done
done

echo "GeoVisio website is now ready !"
exec "$@"
